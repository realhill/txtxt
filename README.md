# 使用自定义脚本进行数据转换的Qt框架

#### 介绍

**** 本文件里的图像在gitee里看不见，也不知道怎么显示出来，另上传了一个readme.pdf文件 ****

把各类数据用自定义脚本转换为需要的数据或展示数据。

发行包里行情数据展示为K线图，文本转换为表格，图像处理...

近段时间闲下来得到这个简单想法，动手写了个简单的框架，怎么想就怎么写，完全没有修饰。希望能对大家有点用。

#### 软件说明

![image-20231013100706120](README.assets\image-20231013100706120.png)



Ast：定义各种接口文件，负责加载管理各个场景模块

ChartScene：输出场景，实现了指标线和K线的展示，可拖动和缩放

lib：flex&bison写的自定义脚本语言（c语言写的），这里只有动态库

TableInScene：输入场景，表格数据展示，简单修改，复制。向脚本提供读取列数据，合并列等功能

TextInScene：输入场景，文本数据展示。向脚本提供读取行和获取行数的功能

TextOutScene：输出场景，文本消息输出。向脚本提供打印Qt多个类型数据

TxTxT：主程序

Worker：脚本工作区

#### Qt环境

win10 ; Qt版本：5.14.1


#### 安装教程

源码下载后直接解压；发行包直接解压后直接运行txtxt.exe。

#### 使用说明

1. 启动后界面

   <img src="README.assets\image-20231013102522695.png" alt="image-20231013102522695" style="zoom:80%;" />

2. 目前是最简单功能，所以要手动增加输入场景、输出场景和工作区<img src="README.assets\image-20231013104729981.png" alt="image-20231013104729981" style="zoom:80%;" />

3. 增加场景后<img src="README.assets\image-20231013105702729.png" alt="image-20231013105702729" style="zoom:80%;" />

4. 导入数据

<img src="README.assets\image-20231013105930393.png" alt="image-20231013105930393" style="zoom:80%;" />

<img src="README.assets\image-20231013110119411.png" alt="image-20231013110119411" style="zoom:80%;" />

5. 写脚本

   <img src="README.assets\image-20231013111326323.png" alt="image-20231013111326323" style="zoom:80%;" />

6. 两列合并

   <img src="README.assets\image-20231013111841589.png" alt="image-20231013111841589" style="zoom:80%;" />

7. 添加指标线

   <img src="README.assets\image-20231013112307130.png" alt="image-20231013112307130" style="zoom:80%;" />

8. 用鼠标可以缩放和拖动K线图

9. 输出消息

   

<img src="README.assets\image-20231013114806035.png" alt="image-20231013114806035" style="zoom:80%;" />
