﻿#include "TextOutScene.h"

#include "TextOutSceneStruct.h"

int createInstance(void**out)
{
	if (out) {
		TextOutSceneStruct*st = new TextOutSceneStruct(nullptr);
		*out = dynamic_cast<AstStruct*>(st);
	}
	return 0;
}
void freeInstance(void*_)
{
	if (_) {
		TextOutSceneStruct*st = (TextOutSceneStruct*)_;
		delete st;
	}
}