﻿#include "TextOutSceneStruct.h"

#include "TextOutSceneWidget.h"
#include "TextOutScenePrintFunction.h"

class TextOutSceneStruct::TextOutSceneStructPrivate :public QObject
{
public:
	TextOutScenePrintFunction * print_function;
public:
	TextOutSceneStructPrivate(QObject*parent) :QObject(parent)
	{
		print_function = new TextOutScenePrintFunction(this);
	}
};

TextOutSceneStruct::TextOutSceneStruct(QObject *parent)
	: QObject(parent), private_ptr(new TextOutSceneStructPrivate(this))
{
}

TextOutSceneStruct::~TextOutSceneStruct()
{
}

//////////////////////////////////////////////////////
//
QString TextOutSceneStruct::sceneNames(int scene_type)
{
	if (AstScene::SCENE_OUT == scene_type)
		return "TextOutScene";

	return "";
}
bool TextOutSceneStruct::hasScene(const QString&name, int scene_type)
{
	switch (scene_type) {
	case AstScene::SCENE_OUT:
		if (0 == name.compare("TextOutScene", Qt::CaseInsensitive))
			return true;
		break;
	}

	return false;
}
AstScene* TextOutSceneStruct::createScene(const QString&name, int scene_type, AstStructContext*ctx)
{
	switch (scene_type) {
	case AstScene::SCENE_OUT:
		if (0 == name.compare("TextOutScene", Qt::CaseInsensitive)) {
			TextOutSceneWidget*wgt = new TextOutSceneWidget(nullptr);
			return wgt ? wgt->operator()() : nullptr;
		}
		break;
	}
	return nullptr;
}
void TextOutSceneStruct::freeScene(AstScene*_)
{
	delete _->widget();
}
QString TextOutSceneStruct::functionNames()
{
	return "TextOutScenePrint";
}
AstFunction* TextOutSceneStruct::getFunction(const QString&name)
{
	if (0 == name.compare("TextOutScenePrint", Qt::CaseSensitivity::CaseInsensitive)) {
		return private_ptr->print_function->operator()();
	}
	return nullptr;
}
