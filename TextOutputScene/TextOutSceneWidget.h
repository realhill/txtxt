﻿#pragma once

#include <QWidget>
namespace Ui { class TextOutSceneWidget; };

class AstScene;
class TextOutSceneWidget : public QWidget
{
	Q_OBJECT
private:
	class TextOutSceneWidgetPrivate;
	TextOutSceneWidgetPrivate*private_ptr;
public:
	TextOutSceneWidget(QWidget *parent = Q_NULLPTR);
	~TextOutSceneWidget();

	AstScene* operator()();
	void appendText(const QString&);
	void insertText(const QString&);
private slots:
	void on_spinBox_valueChanged(int);
private:
	Ui::TextOutSceneWidget *ui;
};
