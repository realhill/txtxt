﻿#include "TextOutSceneWidget.h"
#include "ui_TextOutSceneWidget.h"

#include "GlobalDefine.h"

class TextOutSceneWidget::TextOutSceneWidgetPrivate :public QObject, public AstScene
{
public:
	QString label_name;
public:
	TextOutSceneWidgetPrivate(QObject*parent) :QObject(parent)
	{}

	virtual int type() override
	{
		return AstScene::SCENE_OUT;
	}
	virtual QString typeName() override
	{
		return "TextOutScene";
	}
	virtual QString label()
	{
		return label_name;
	}
	virtual void setLabel(const QString&s)
	{
		label_name = s;
	}
	virtual QWidget* widget() override
	{
		return qobject_cast<QWidget*>(parent());
	}
	virtual void onOpening() override
	{}
	virtual void onClosing() override
	{}

};

TextOutSceneWidget::TextOutSceneWidget(QWidget *parent)
	: QWidget(parent),private_ptr(new TextOutSceneWidgetPrivate(this))
{
	ui = new Ui::TextOutSceneWidget();
	ui->setupUi(this);
}

TextOutSceneWidget::~TextOutSceneWidget()
{
	delete ui;
}

AstScene* TextOutSceneWidget::operator()()
{
	return dynamic_cast<AstScene*>(private_ptr);
}
void TextOutSceneWidget::appendText(const QString&s)
{
	ui->edt_msg->appendPlainText(s);
}
void TextOutSceneWidget::insertText(const QString&s)
{
	QTextCursor prev_cursor = ui->edt_msg->textCursor();
	ui->edt_msg->moveCursor(QTextCursor::End);
	ui->edt_msg->insertPlainText(s);
	ui->edt_msg->setTextCursor(prev_cursor);
}
void TextOutSceneWidget::on_spinBox_valueChanged(int n)
{
	ui->edt_msg->setMaximumBlockCount(n);
}
