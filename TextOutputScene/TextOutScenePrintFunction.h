﻿#pragma once

#include <QObject>

#include "GlobalDefine.h"

class TextOutScenePrintFunction : public QObject, public AstFunction
{
	Q_OBJECT
private:
	class TextOutScenePrintFunctionPrivate;
	TextOutScenePrintFunctionPrivate*private_ptr;

public:
	TextOutScenePrintFunction(QObject *parent);
	~TextOutScenePrintFunction();

public:
	AstFunction* operator()();
	// AstFunction
public:
	virtual QString name() override;
	virtual int* outputTypes() override;
	virtual int outputCount() override;
	virtual int* inputTypes() override;
	virtual int inputCount() override;
	virtual void* function() override;

};
