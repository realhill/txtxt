﻿#include "TxInWidget.h"
#include "ui_TxInWidget.h"

#include <QFileDialog>
#include <QFile>
#include <QFileInfo>
#include <QTextCodec>
#include <QTextStream>
#include <QByteArray>
#include <QByteArrayList>
#include <QDebug>

#include "GlobalDefine.h"
#include "GlobalData.h"
#include "TxOutWidget.h"

class TxInWidget::TxInWidgetPrivate :public QObject
{
public:
	int scene_count;
public:
	TxInWidgetPrivate(QObject*parent):QObject(parent)
	{
		scene_count = 0;
	}
	int newSceneID()
	{
		return ++scene_count;
	}
	// AstIn重写
public:

};
TxInWidget::TxInWidget(QWidget *parent)
	: QWidget(parent), private_ptr(new TxInWidgetPrivate(this))
{
	ui = new Ui::TxInWidget();
	ui->setupUi(this);

	QStringList lst = GlobalData::instance()->ast()->sceneNames(AstScene::SCENE_IN).split(",");

	ui->cbx_scene->addItems(lst);
	//ui->cbx_scene->insertItem(0, "TextInScene");
	//ui->cbx_scene->insertItem(1, "TableInScene");

	ui->tabWidget->tabBar()->setTabButton(0, QTabBar::RightSide, nullptr);
}

TxInWidget::~TxInWidget()
{
	delete ui;
}

//AstIn* TxInWidget::operator()()
//{
//	return qobject_cast<AstIn*>(private_ptr);
//}

static int tabIndexOf(QTabWidget*p, const QString&s)
{
	int count = p->count();
	for (int i = 0; i < count; i++) {
		if (s == p->tabText(i)) {
			return i;
		}
	}
	return -1;
}

int TxInWidget::queryScene(const QString&scene_name, int scene_type, QObject**out)
{
	int idx = tabIndexOf(ui->tabWidget, scene_name);
	if (idx < 0) {
		if (out) *out = nullptr;
		return 0;
	}

	QWidget*wgt = ui->tabWidget->widget(idx);
	AstScene*scene = (AstScene*)wgt->userData(0);
	if (scene->type() == scene_type) {
		if (out) *out = (QObject*)scene;
	}

	return 0;
}
int TxInWidget::openScene(const QString&scene_name, int scene_type, QObject**out)
{
	return 0;
}
int TxInWidget::closeScene(const QString&scene_name)
{
	return 0;
}


void TxInWidget::on_btn_newscene_clicked()
{
	QString name = ui->cbx_scene->currentText();
	QString label = ui->edt_scenename->text();
	if (label.isEmpty())
		label = QString("in-%1").arg(private_ptr->newSceneID());

	int idx = tabIndexOf(ui->tabWidget, label);
	if (idx >= 0) {
		ui->tabWidget->setCurrentIndex(idx);
		return;
	}

	AstScene* as = GlobalData::instance()->ast()->createScene(name, AstScene::SCENE_IN, label);
	//as->widget()->setUserData(0, (QObjectUserData*)as);
	idx = ui->tabWidget->addTab(as->widget(), label);
	ui->tabWidget->setCurrentIndex(idx);


	//if ("TextInScene" == name) {
	//	TextInSceneWidget*p = new TextInSceneWidget(this);
	//	p->setUserData(0, (QObjectUserData*)(TextInScene*)p);
	//	int idx = ui->tabWidget->addTab(p, label);
	//	ui->tabWidget->setCurrentIndex(idx);
	//}
	//else if ("TableInScene" == name) {
	//	TableInSceneWidget*p = new TableInSceneWidget(this);
	//	p->setUserData(0, (QObjectUserData*)(TableInScene*)p);
	//	int idx = ui->tabWidget->addTab(p, label);
	//	ui->tabWidget->setCurrentIndex(idx);
	//}
}

void TxInWidget::on_tabWidget_tabCloseRequested(int idx)
{
	QString s = ui->tabWidget->tabText(idx);
	GlobalData::instance()->ast()->closeScene(AstScene::SCENE_IN, s);
	ui->tabWidget->removeTab(idx);
}