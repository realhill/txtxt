﻿#pragma once

#include <QWidget>
namespace Ui { class TxWorkerWidget; };

class AstWorker;
class TxWorkerWidget : public QWidget
{
	Q_OBJECT
private:
	class TxWorkerWidgetPrivate;
	TxWorkerWidgetPrivate*private_ptr;
public:
	TxWorkerWidget(QWidget *parent = Q_NULLPTR);
	~TxWorkerWidget();

	//void setAstWorker(AstWorker*);
private slots:
	void on_btn_create_clicked();
	void on_tabWidget_tabCloseRequested(int);
	void on_tabWidget_tabBarDoubleClicked(int);

private:
	Ui::TxWorkerWidget *ui;
};
