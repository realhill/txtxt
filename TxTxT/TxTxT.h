﻿#pragma once

#include <QtWidgets/QMainWindow>
#include "ui_TxTxT.h"

class TxTxT : public QMainWindow
{
    Q_OBJECT

public:
    TxTxT(QWidget *parent = Q_NULLPTR);

private slots:
	void on_action_file_script_triggered();
	void on_action_file_scenein_triggered();
	void on_action_file_sceneout_triggered();
	void on_action_view_input_triggered();
	void on_action_view_output_triggered();
	//
	void on_dockInWidget_visibilityChanged(bool visible);
	void on_dockOutWidget_visibilityChanged(bool visible);
	void on_dockInWidget_topLevelChanged(bool);
	void on_dockOutWidget_topLevelChanged(bool);
	//
	//void on_tabWidget_tabCloseRequested(int);

private:
    Ui::TxTxTClass ui;
};
