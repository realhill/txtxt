﻿#include "TxTxT.h"
#include "TxOutWidget.h"
#include "TxInWidget.h"
#include "TxWorkerWidget.h"

#include "GlobalData.h"

#include <QLabel>
#include <QDir>
#include <QTabBar>

TxTxT::TxTxT(QWidget *parent)
    : QMainWindow(parent)
{
    ui.setupUi(this);

	GlobalData::instance()->init(QApplication::applicationDirPath());

	TxOutWidget*out = new TxOutWidget(this);
	ui.dockOutWidget->setWidget(out);
	TxInWidget*in = new TxInWidget(this);
	ui.dockInWidget->setWidget(in);

	TxWorkerWidget*worker = new TxWorkerWidget(this);
	this->centralWidget()->layout()->addWidget(worker);

	//GlobalData::instance()->setInWidget(in);
	//GlobalData::instance()->setOutWidget(out);

	QLabel*lbl_info = new QLabel(QStringLiteral(" 信息 "), this);
	lbl_info->setFrameShape(QFrame::Shape::Panel);
	lbl_info->setFrameShadow(QFrame::Shadow::Sunken);
	ui.statusBar->addWidget(lbl_info);

	QLabel*lbl_msg = new QLabel(QStringLiteral(" 消息 "), this);
	lbl_msg->setFrameShape(QFrame::Shape::Panel);
	lbl_msg->setFrameShadow(QFrame::Shadow::Sunken);
	ui.statusBar->addWidget(lbl_msg, 1);

	//ui.tabWidget->tabBar()->setTabButton(0, QTabBar::RightSide, nullptr);

	connect(ui.dockInWidget, SIGNAL(visibilityChanged(bool)), this, SLOT(on_dockInWidget_visibilityChanged(bool)));
	connect(ui.dockInWidget, SIGNAL(topLevelChanged(bool)), this, SLOT(on_dockInWidget_topLevelChanged(bool)));

	connect(ui.dockOutWidget, SIGNAL(visibilityChanged(bool)), this, SLOT(on_dockOutWidget_visibilityChanged(bool)));
	connect(ui.dockOutWidget, SIGNAL(topLevelChanged(bool)), this, SLOT(on_dockOutWidget_topLevelChanged(bool)));


}

void TxTxT::on_action_file_script_triggered()
{
}

void TxTxT::on_action_file_scenein_triggered()
{
}
void TxTxT::on_action_file_sceneout_triggered()
{
}
void TxTxT::on_action_view_input_triggered()
{
	bool checked = ui.action_view_input->isChecked();
	if (checked) {
		ui.dockInWidget->show();
	}
	else {
		ui.dockInWidget->hide();
	}
	ui.action_view_input->setChecked(checked);
}
void TxTxT::on_action_view_output_triggered()
{
	bool checked = ui.action_view_output->isChecked();
	if (checked) {
		ui.dockOutWidget->show();
	}
	else {
		ui.dockOutWidget->hide();
	}
	ui.action_view_output->setChecked(checked);

}
void TxTxT::on_dockInWidget_visibilityChanged(bool visible)
{
	ui.action_view_input->setChecked(visible);//->setChecked(visible);
}
void TxTxT::on_dockOutWidget_visibilityChanged(bool visible)
{
	ui.action_view_output->setChecked(visible);//->setChecked(visible);
}
void TxTxT::on_dockInWidget_topLevelChanged(bool)
{}
void TxTxT::on_dockOutWidget_topLevelChanged(bool)
{}

//void TxTxT::on_tabWidget_tabCloseRequested(int idx)
//{
//	QString s = ui.tabWidget->tabText(idx);
//	//GlobalData::instance()->ast()->closeScene(AstScene::SCENE_IN, s);
//	ui.tabWidget->removeTab(idx);
//}
