﻿#pragma once

#include <QWidget>
namespace Ui { class TxInWidget; };

//class AstIn;
class TxInWidget : public QWidget
{
	Q_OBJECT
private:
	class TxInWidgetPrivate;
	TxInWidgetPrivate*private_ptr;
public:
	TxInWidget(QWidget *parent = Q_NULLPTR);
	~TxInWidget();

public:
	int queryScene(const QString&scene_name, int scene_type, QObject**out);
	int openScene(const QString&scene_name, int scene_type, QObject**out);
	int closeScene(const QString&scene_name);

private slots:
	void on_btn_newscene_clicked();
	void on_tabWidget_tabCloseRequested(int);
private:
	Ui::TxInWidget *ui;
};
