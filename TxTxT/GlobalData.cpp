﻿#include "GlobalData.h"

#include <QDir>
//#include "TxInWidget.h"
//#include "TxOutWidget.h"

#include "GlobalDefine.h"

static GlobalData global_data;

class GlobalData::GlobalDataPrivate :public QObject
{
public:
	//TxInWidget * in_widget;
	//TxOutWidget * out_widget;
	Ast * ast;
public:
	GlobalDataPrivate(QObject*parent):QObject(parent)
	{
		//in_widget = nullptr;
		//out_widget = nullptr;
		ast = nullptr;
	}
};

GlobalData::GlobalData(QObject *parent)
	: QObject(parent),private_ptr(new GlobalDataPrivate(this))
{
}

GlobalData::~GlobalData()
{
}

GlobalData* GlobalData::instance()
{
	return &global_data;
}

void GlobalData::init(const QString&root_path)
{
	Ast* p = new Ast(this);

	int r = p->init(root_path + QDir::separator() + "dll");

	private_ptr->ast = p;
}

Ast* GlobalData::ast()
{
	return private_ptr->ast;
}

//void GlobalData::setInWidget(TxInWidget*in)
//{
//	private_ptr->in_widget = in;
//}
//TxInWidget* GlobalData::inWidget()
//{
//	return private_ptr->in_widget;
//}
//void GlobalData::setOutWidget(TxOutWidget*out)
//{
//	private_ptr->out_widget = out;
//}
//TxOutWidget* GlobalData::outWidget()
//{
//	return private_ptr->out_widget;
//}
