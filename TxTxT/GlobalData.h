﻿#pragma once

#include <QObject>

//class TxInWidget;
//class TxOutWidget;
class Ast;
class GlobalData : public QObject
{
	Q_OBJECT
private:
	class GlobalDataPrivate;
	GlobalDataPrivate*private_ptr;
public:
	GlobalData(QObject *parent = nullptr);
	~GlobalData();

	static GlobalData* instance();

public:
	void init(const QString&root_path);
	Ast* ast();
	//void setInWidget(TxInWidget*in);
	//TxInWidget* inWidget();
	//void setOutWidget(TxOutWidget*out);
	//TxOutWidget* outWidget();
};
