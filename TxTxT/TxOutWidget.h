﻿#pragma once

#include <QWidget>
namespace Ui { class TxOutWidget; };

class TxOutWidget : public QWidget
{
	Q_OBJECT
private:
	class TxOutWidgetPrivate;
	TxOutWidgetPrivate*private_ptr;

public:
	TxOutWidget(QWidget *parent = Q_NULLPTR);
	~TxOutWidget();

public:
	void appendOutput(const QString&m);
public:
	int queryScene(const QString&scene_name, int scene_type, QObject**out);
	int openScene(const QString&scene_name, int scene_type, QObject**out);
	int closeScene(const QString&scene_name);

private slots:
	void on_btn_newscene_clicked();
	void on_tabWidget_tabCloseRequested(int);

private:
	Ui::TxOutWidget *ui;
};

#define PRINT_OUTPUT(m) GlobalData::instance()->outWidget()->appendOutput(m) 