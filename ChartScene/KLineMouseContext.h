﻿#pragma once

#include <QObject>

#include "KDefine.h"

class KLineCanvas;
class KLineView;
//class KLineCol;
class KLineMouseContext
{
	QPointF		_pos;
public:
	KLineCanvas		* canvas;
	KLineView		* view;
	int				col_idx;
	//int			view_idx;
	//int			item_idx;// 索引
	int				area_id;
	void			* data;
public:
	KLineMouseContext()
	{
		reset();
	}
	void reset()
	{
		_pos = QPointF();
		area_id = VIEWAREA_NONE;
		col_idx = -1;
		view = nullptr;
		//view_idx = -1;
		//item_idx = -1;
		//kline_idx = -1;
		data = nullptr;
	}
public:
	void setPos(const QPointF&pt)
	{
		_pos = pt;
	}
	const QPointF& pos() const
	{
		return _pos;// .toPoint();
	}
};
