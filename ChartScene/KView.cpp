﻿#include "KView.h"

#include <QPainter>
#include <QPaintEvent>

//#include "KMouseContext.h"

class KView::KViewPrivate:public QObject
{
public:
	int view_id;
	QRectF rect;
	QString name;
public:
	KViewPrivate(QObject*parent):QObject(parent)
	{
		view_id = 0;
	}
};

KView::KView(QObject *parent)
	: QObject(parent), private_ptr(new KViewPrivate(this))
{
}

KView::~KView()
{
}

int KView::id()
{
	return private_ptr->view_id;
}

void KView::setId(int n)
{
	private_ptr->view_id = n;
}
QString KView::name()
{
	return private_ptr->name;
}
void KView::setName(const QString&_)
{
	private_ptr->name = _;
}
QRectF KView::rect()
{
	return private_ptr->rect;
}
void KView::setRect(const QRectF&_)
{
	private_ptr->rect = _;
}
void KView::draw(QPainter*p, QPaintEvent *event)
{
	p->save();
	onPaint(p, event);
	p->restore();
}
void KView::onPaint(QPainter*p, QPaintEvent *event)
{}
