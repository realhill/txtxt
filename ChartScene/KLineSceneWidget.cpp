﻿#include "KLineSceneWidget.h"
#include "ui_KLineSceneWidget.h"

#include "GlobalDefine.h"

#include "KLineCanvas.h"

class KLineSceneWidget::KLineSceneWidgetPrivate :public QObject, public AstScene
{
public:
	QString label_name;
	KLineCanvas * canvas;
public:
	KLineSceneWidgetPrivate(QObject*parent) :QObject(parent)
	{
		canvas = new KLineCanvas(this);
		canvas->attachWidget((QWidget*)parent);
		canvas->setMargin(100, 100, 20, 40);
		canvas->setBorderVisible(1, 0, 0, 1);
	}

	virtual int type() override
	{
		return AstScene::SCENE_OUT;
	}
	virtual QString typeName() override
	{
		return "KLineScene";
	}
	virtual QString label()
	{
		return label_name;
	}
	virtual void setLabel(const QString&s)
	{
		label_name = s;
	}
	virtual QWidget* widget() override
	{
		return qobject_cast<QWidget*>(parent());
	}
	virtual void onOpening() override
	{}
	virtual void onClosing() override
	{}

};

KLineSceneWidget::KLineSceneWidget(QWidget *parent)
	: QWidget(parent), private_ptr(new KLineSceneWidgetPrivate(this))
{
	ui = new Ui::KLineSceneWidget();
	ui->setupUi(this);
}

KLineSceneWidget::~KLineSceneWidget()
{
	delete ui;
}

AstScene* KLineSceneWidget::operator()()
{
	return dynamic_cast<AstScene*>(private_ptr);
}

KLineCanvas*KLineSceneWidget::canvas()
{
	return private_ptr->canvas;
}
