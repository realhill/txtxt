﻿#include "KLineAddLayerFunction.h"

#include <QVector>
#include <QVariant>

#include "KLineSceneWidget.h"
#include "KLineCanvas.h"
#include "KLineView.h"
#include "KLineLayer.h"
#include "KIndicatorLayer.h"
#include "KColorStyle.h"
#include "../TableInScene/VectorInterface.h"

class KLineAddLayerFunction::KLineAddLayerFunctionPrivate :QObject
{
public:
	int in_params[16];
	int out_params[1];
	int in_count;
	int out_count;
public:
	KLineAddLayerFunctionPrivate(QObject*parent) :QObject(parent)
	{
		in_count = 4;
		out_count = 1;

		in_params[0] = FPT_PTR;				// scene pointer
		in_params[1] = FPT_STRING;			// view name
		in_params[2] = FPT_STRING;			// layer name
		in_params[3] = FPT_VECTOR;				// vector<variant>*
		out_params[0] = FPT_NUMBER;			// return 0|1
	}
};

KLineAddLayerFunction::KLineAddLayerFunction(QObject *parent)
	: QObject(parent), private_ptr(new KLineAddLayerFunctionPrivate(this))
{
}

KLineAddLayerFunction::~KLineAddLayerFunction()
{
}

static int KLineAddLayerFunction_FUNCTION(struct variant_s *in, int in_count, struct variant_s *out, void*tag)
{
	if (4 != in_count)
		return ERR_PARAM_NOT_MATCH;

	// 判断第一个参数
	if (VALUETYPE_VOID_PTR != in[0].type)
		return ERR_DATATYPE_NOT_MATCH;
	if (nullptr == in[0].u.ptr)
		return ERR_DATATYPE_NOT_MATCH;

	AstScene*as = nullptr;
	KLineSceneWidget*ptr = nullptr;
	try {
		as = (AstScene*)in[0].u.ptr;
		ptr = dynamic_cast<KLineSceneWidget*>(as->widget());
		//doc = ptr->textDocument();
	}
	catch (...) {
		//doc = nullptr;
	}

	if (!ptr)
		return -1;

	// 第二个参数
	struct astring_s*str = (struct astring_s*)in[1].u.ptr;
	QString view_name = str->value;
	// 第三个参数
	str = (struct astring_s*)in[2].u.ptr;
	QString layer_name = str->value;
	// 第四个参数
	IVector*iv = (IVector*)in[3].u.ptr;
	//QString layer_name = str->value;

	KLineCanvas*canvas = ptr->canvas();

	KLineView*view = canvas->view(view_name);
	KLineLayer*l = view->layer(layer_name);
	if (!l) {
		KIndicatorLayer*pk = new KIndicatorLayer(view);
		pk->setName(layer_name);
		//pk->setViewContext(view->context());
		pk->setColor(KColorStyle::KCOLOR_INDICATOR_LINE1 + view->layerCount() % 10);
		view->addLayer(pk);
		l = pk;
	}
	l->setData(0, iv->data());
	canvas->recalc();
	canvas->update();

	
	//DoubleVector*pv = new DoubleVector(nullptr);
	//out->type = VALUETYPE_DOUBLE_VECTOR;
	//out->u.ptr = (IVector*)pv;
	//out->fn_addref = BaseVector::addref_fn; //addref_doublevector;
	//out->fn_release = BaseVector::release_fn;//release_doublevector;

	//qDebug() << "double vector:" << out->u.ptr << ", fn:" << BaseVector::addref_fn << "," << BaseVector::release_fn;

	return 0;
}

AstFunction* KLineAddLayerFunction::operator()()
{
	return dynamic_cast<AstFunction*>(this);
}
QString KLineAddLayerFunction::ID()
{
	return "KLineAddLayer";
}
QString KLineAddLayerFunction::name()
{
	return ID();
}
int* KLineAddLayerFunction::outputTypes()
{
	return private_ptr->out_params;
}
int KLineAddLayerFunction::outputCount()
{
	return private_ptr->out_count;
}
int* KLineAddLayerFunction::inputTypes()
{
	return private_ptr->in_params;
}
int KLineAddLayerFunction::inputCount()
{
	return private_ptr->in_count;
}
void* KLineAddLayerFunction::function()
{
	return KLineAddLayerFunction_FUNCTION;
}
