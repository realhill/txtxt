﻿#pragma once

#include <QObject>
#include <QRect>

class KColorStyle;
class KLineMouseContext;
class KLineCanvas;

class KLineCanvasContext
{
public:
	enum {
		MOUSEMODE_NONE = 0,
		MOUSEMODE_DRAG,
		MOUSEMODE_QUERY,
	};
public:
	KLineCanvas			* canvas;
	KLineMouseContext	* mouse_context;
	KLineMouseContext	* mousedown_context;
	KLineMouseContext	* lastmouse_context;
	KColorStyle			* color_style;
	QRectF				rect_firstcol;
	int					mouse_mode;	// 鼠标模式
	int					barwidthstyle_idx;
	int					canvas_colwidth;
	int					canvas_colcount;
	int					item_startidx;		// 数据起始索引
	int					mouse_colidx;		// 鼠标所在列索引
	//int					item_idx;
public:
	KLineCanvasContext()
	{
		canvas = nullptr;
		mouse_context = nullptr;
		mousedown_context = nullptr;
		lastmouse_context = nullptr;
		color_style = nullptr;
		mouse_mode = MOUSEMODE_NONE;
		mouse_colidx = -1;
		item_startidx = -1;
		barwidthstyle_idx = -1;
		canvas_colwidth = 0;
		canvas_colcount = 0;
	}
public:

};
