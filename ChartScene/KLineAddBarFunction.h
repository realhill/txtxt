﻿#pragma once

#include <QObject>
#include "GlobalDefine.h"

class KLineAddBarFunction : public QObject, public AstFunction
{
	Q_OBJECT
private:
	class KLineAddBarFunctionPrivate;
	KLineAddBarFunctionPrivate*private_ptr;

public:
	KLineAddBarFunction(QObject *parent);
	~KLineAddBarFunction();

	static QString ID();

public:
	AstFunction* operator()();
	// AstFunction
public:
	virtual QString name() override;
	virtual int* outputTypes() override;
	virtual int outputCount() override;
	virtual int* inputTypes() override;
	virtual int inputCount() override;
	virtual void* function() override;

};
