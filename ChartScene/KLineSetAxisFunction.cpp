﻿#include "KLineSetAxisFunction.h"

#include <QVector>
#include <QVariant>

#include "KLineSceneWidget.h"
#include "KLineCanvas.h"
//#include "KColorStyle.h"
#include "KLineAxisView.h"
#include "KLineAxisLayer.h"

#include "../TableInScene/VectorInterface.h"

class KLineSetAxisFunction::KLineSetAxisFunctionPrivate :QObject
{
public:
	int in_params[16];
	int out_params[1];
	int in_count;
	int out_count;
public:
	KLineSetAxisFunctionPrivate(QObject*parent) :QObject(parent)
	{
		in_count = 3;
		out_count = 1;

		in_params[0] = FPT_PTR;				// scene pointer
		in_params[1] = FPT_VECTOR;			// view name
		in_params[2] = FPT_STRING;			// date format
		out_params[0] = FPT_NUMBER;			// return 0|1
	}
};

KLineSetAxisFunction::KLineSetAxisFunction(QObject *parent)
	: QObject(parent),private_ptr(new KLineSetAxisFunctionPrivate(this))
{
}

KLineSetAxisFunction::~KLineSetAxisFunction()
{
}

static int KLineSetAxisFunction_FUNCTION(struct variant_s *in, int in_count, struct variant_s *out, void*tag)
{
	if (3 != in_count)
		return ERR_PARAM_NOT_MATCH;

	// 判断第一个参数
	if (VALUETYPE_VOID_PTR != in[0].type)
		return ERR_DATATYPE_NOT_MATCH;
	if (nullptr == in[0].u.ptr)
		return ERR_DATATYPE_NOT_MATCH;

	AstScene*as = nullptr;
	KLineSceneWidget*ptr = nullptr;
	try {
		as = (AstScene*)in[0].u.ptr;
		ptr = dynamic_cast<KLineSceneWidget*>(as->widget());
	}
	catch (...) {}

	if (!ptr)
		return -1;

	// 第二个参数
	IVector*iv = (IVector*)in[1].u.ptr;
	// 第三个参数
	struct astring_s*str = (struct astring_s*)in[2].u.ptr;
	QString fmt = str->value;


	KLineCanvas*canvas = ptr->canvas();
	KLineAxisView*axis = canvas->axisView();
	KLineAxisLayer*l = (KLineAxisLayer*)axis->mainLayer();
	if (nullptr == l) {
		l = new KLineAxisLayer(axis);
		axis->setMainLayer(l);
	}
	l->setDate(iv->data(), fmt);
	canvas->recalc();
	canvas->update();

	return 0;
}

AstFunction* KLineSetAxisFunction::operator()()
{
	return dynamic_cast<AstFunction*>(this);
}
QString KLineSetAxisFunction::ID()
{
	return "KLineSetAxis";
}
QString KLineSetAxisFunction::name()
{
	return ID();
}
int* KLineSetAxisFunction::outputTypes()
{
	return private_ptr->out_params;
}
int KLineSetAxisFunction::outputCount()
{
	return private_ptr->out_count;
}
int* KLineSetAxisFunction::inputTypes()
{
	return private_ptr->in_params;
}
int KLineSetAxisFunction::inputCount()
{
	return private_ptr->in_count;
}
void* KLineSetAxisFunction::function()
{
	return KLineSetAxisFunction_FUNCTION;
}