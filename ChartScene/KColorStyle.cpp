﻿#include "KColorStyle.h"

#include <QMap>
#include <QVector>

class KColorStyle::KColorStylePrivate : public QObject
{
public:
	QVector<QColor> globalblack_arr;
	QVector<QColor> globalwhite_arr;
	QVector<QColor>*globalcolor_arr;// &globalwhite_arr;

public:
	KColorStylePrivate(QObject*parent) : QObject(parent)
	{
		globalcolor_arr = nullptr;
	}
};
KColorStyle::KColorStyle(QObject *parent)
	: QObject(parent),private_ptr(new KColorStylePrivate(this))
{
	init();
}

KColorStyle::~KColorStyle()
{
}

void KColorStyle::setGlobalColor(QVector<QColor> * arr, int idx, const QColor& clr)
{
	if (idx < 0 || idx >= arr->count())
		return;
	arr->operator[](idx) = clr;
}
QColor KColorStyle::globalColor(const QVector<QColor> * arr, int idx)
{
	if (idx < 0 || idx >= arr->count())
		return QColor();
	return arr->operator[](idx);
}

void KColorStyle::init()
{
	if (!private_ptr->globalcolor_arr) {

		private_ptr->globalblack_arr.resize(KColorStyle::KCOLOR_MAX);

		QVector<QColor>& globalblack_arr = private_ptr->globalblack_arr;

		setGlobalColor(&globalblack_arr, KColorStyle::KCOLOR_BACKGROUND, Qt::black);
		setGlobalColor(&globalblack_arr, KColorStyle::KCOLOR_TITLE, QColor(231, 231, 231));
		setGlobalColor(&globalblack_arr, KColorStyle::KCOLOR_TEXT, QColor(231, 231, 231));
		setGlobalColor(&globalblack_arr, KColorStyle::KCOLOR_WARN, QColor(220, 220, 10));
		setGlobalColor(&globalblack_arr, KColorStyle::KCOLOR_AXIS, QColor(132, 0, 0));
		setGlobalColor(&globalblack_arr, KColorStyle::KCOLOR_AXIS_SCALE, QColor(128, 128, 128));
		setGlobalColor(&globalblack_arr, KColorStyle::KCOLOR_BORDER, QColor(190, 20, 10));
		setGlobalColor(&globalblack_arr, KColorStyle::KCOLOR_KLINE_RED, QColor(255, 60, 60));
		setGlobalColor(&globalblack_arr, KColorStyle::KCOLOR_KLINE_GREEN, QColor(0, 240, 240));
		setGlobalColor(&globalblack_arr, KColorStyle::KCOLOR_KLINE_WHITE, QColor(231, 231, 231));
		setGlobalColor(&globalblack_arr, KColorStyle::KCOLOR_INDICATOR_NAME, QColor(220, 220, 10));
		setGlobalColor(&globalblack_arr, KColorStyle::KCOLOR_INDICATOR_LINE1, QColor(231, 231, 231));
		setGlobalColor(&globalblack_arr, KColorStyle::KCOLOR_INDICATOR_LINE2, QColor(220, 220, 10));
		setGlobalColor(&globalblack_arr, KColorStyle::KCOLOR_INDICATOR_LINE3, QColor(225, 0, 255));
		setGlobalColor(&globalblack_arr, KColorStyle::KCOLOR_INDICATOR_LINE4, QColor(0, 240, 0));
		setGlobalColor(&globalblack_arr, KColorStyle::KCOLOR_INDICATOR_LINE5, QColor(120, 120, 120));
		setGlobalColor(&globalblack_arr, KColorStyle::KCOLOR_INDICATOR_LINE6, QColor(255, 60, 60));
		setGlobalColor(&globalblack_arr, KColorStyle::KCOLOR_INDICATOR_LINE7, QColor(255, 128, 0));
		setGlobalColor(&globalblack_arr, KColorStyle::KCOLOR_INDICATOR_LINE8, QColor(128, 128, 0));
		setGlobalColor(&globalblack_arr, KColorStyle::KCOLOR_INDICATOR_LINE9, QColor(128, 0, 128));
		setGlobalColor(&globalblack_arr, KColorStyle::KCOLOR_INDICATOR_LINE10, QColor(0, 128, 128));

		QVector<QColor>& globalwhite_arr = private_ptr->globalwhite_arr;
		globalwhite_arr.resize(KColorStyle::KCOLOR_MAX);
		setGlobalColor(&globalwhite_arr, KColorStyle::KCOLOR_BACKGROUND, QColor(245, 245, 245));
		setGlobalColor(&globalwhite_arr, KColorStyle::KCOLOR_TITLE, QColor(40, 40, 40));
		setGlobalColor(&globalwhite_arr, KColorStyle::KCOLOR_TEXT, QColor(40, 40, 40));
		setGlobalColor(&globalwhite_arr, KColorStyle::KCOLOR_WARN, QColor(0, 0, 150));
		setGlobalColor(&globalwhite_arr, KColorStyle::KCOLOR_AXIS, QColor(192, 192, 192));
		setGlobalColor(&globalwhite_arr, KColorStyle::KCOLOR_AXIS_SCALE, QColor(128, 128, 128));
		setGlobalColor(&globalwhite_arr, KColorStyle::KCOLOR_BORDER, QColor(128, 128, 128));
		setGlobalColor(&globalwhite_arr, KColorStyle::KCOLOR_KLINE_RED, QColor(240, 0, 0));
		setGlobalColor(&globalwhite_arr, KColorStyle::KCOLOR_KLINE_GREEN, QColor(0, 120, 0));
		setGlobalColor(&globalwhite_arr, KColorStyle::KCOLOR_KLINE_WHITE, QColor(0, 0, 0));
		setGlobalColor(&globalwhite_arr, KColorStyle::KCOLOR_INDICATOR_NAME, QColor(47, 94, 224));
		setGlobalColor(&globalwhite_arr, KColorStyle::KCOLOR_INDICATOR_LINE1, QColor(0, 0, 0));
		setGlobalColor(&globalwhite_arr, KColorStyle::KCOLOR_INDICATOR_LINE2, QColor(47, 94, 224));
		setGlobalColor(&globalwhite_arr, KColorStyle::KCOLOR_INDICATOR_LINE3, QColor(225, 0, 255));
		setGlobalColor(&globalwhite_arr, KColorStyle::KCOLOR_INDICATOR_LINE4, QColor(0, 120, 0));
		setGlobalColor(&globalwhite_arr, KColorStyle::KCOLOR_INDICATOR_LINE5, QColor(120, 120, 120));
		setGlobalColor(&globalwhite_arr, KColorStyle::KCOLOR_INDICATOR_LINE6, QColor(240, 0, 0));
		setGlobalColor(&globalwhite_arr, KColorStyle::KCOLOR_INDICATOR_LINE7, QColor(255, 128, 0));
		setGlobalColor(&globalwhite_arr, KColorStyle::KCOLOR_INDICATOR_LINE8, QColor(128, 128, 0));
		setGlobalColor(&globalwhite_arr, KColorStyle::KCOLOR_INDICATOR_LINE9, QColor(128, 0, 128));
		setGlobalColor(&globalwhite_arr, KColorStyle::KCOLOR_INDICATOR_LINE10, QColor(0, 128, 128));

		private_ptr->globalcolor_arr = &private_ptr->globalblack_arr;
	}

}
QColor KColorStyle::color(int idx)
{
	return globalColor(private_ptr->globalcolor_arr, idx);
}

void KColorStyle::setColor(int idx, QColor clr)
{
	setGlobalColor(private_ptr->globalcolor_arr, idx, clr);
}
void KColorStyle::selectStyle(const QString&style_name)
{
	if (style_name.toLower() == "black")
		private_ptr->globalcolor_arr = &private_ptr->globalblack_arr;
	else
		private_ptr->globalcolor_arr = &private_ptr->globalwhite_arr;
}

void KColorStyle::changeStyle()
{
	if (private_ptr->globalcolor_arr != &private_ptr->globalblack_arr)
		private_ptr->globalcolor_arr = &private_ptr->globalblack_arr;
	else
		private_ptr->globalcolor_arr = &private_ptr->globalwhite_arr;
}