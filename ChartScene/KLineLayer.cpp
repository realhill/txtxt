﻿#include "KLineLayer.h"
#include <QPainter>

#include "KLineCanvasContext.h"
#include "KLineViewContext.h"
#include "KColorStyle.h"
#include "KBarWidthStyle.h"

class KLineLayer::KLineLayerPrivate :public QObject
{
public:
	KLineViewContext*view_context;
	int globalcolor_index;
	QColor line_color; 
public:
	KLineLayerPrivate(QObject*parent):QObject(parent)
	{
		view_context = nullptr;
		globalcolor_index = -1;
	}
};

KLineLayer::KLineLayer(QObject *parent)
	: KLayer(parent),private_ptr(new KLineLayerPrivate(this))
{
	setType(KLayer::LAYER_TYPE_NONE);
}

KLineLayer::~KLineLayer()
{
}
KLineViewContext*KLineLayer::viewContext()
{
	return private_ptr->view_context;
}
void KLineLayer::setViewContext(KLineViewContext*_)
{
	private_ptr->view_context = _;
}

void KLineLayer::setColor(int global_idx)
{
	private_ptr->globalcolor_index = global_idx;
}
void KLineLayer::setColor(const QColor&clr)
{
	private_ptr->globalcolor_index = -1;
	private_ptr->line_color = clr;
}
QColor KLineLayer::color()
{
	if (private_ptr->globalcolor_index < 0)
		return private_ptr->line_color;

	return private_ptr->view_context->canvas_context->color_style->color(private_ptr->globalcolor_index);
}
static void drawBar(QPainter*p, QRectF rc, KBarWidthStyleItem*item, int pos_open, int pos_close, int pos_high, int pos_low, const QColor& clr)
{
	int line_x = rc.left() + item->center();
	rc.setLeft(rc.left() + item->left_space);
	//rc.setWidth(item->inner_width);
	rc.setRight(rc.right() - item->right_space);
	QLineF line_up, line_down;
	if (pos_open > pos_close) { // 空心
		rc.setTop(pos_close);
		rc.setBottom(pos_open);
		p->setBrush(Qt::BrushStyle::NoBrush);
		line_up.setLine(line_x, pos_high, line_x, pos_close);
		line_down.setLine(line_x, pos_open ,line_x, pos_low);
	}
	else {
		rc.setTop(pos_open);
		rc.setBottom(pos_close);
		p->setBrush(clr);
		line_up.setLine(line_x, pos_high, line_x, pos_open);
		line_down.setLine(line_x, pos_close ,line_x, pos_low);
	}

	p->setPen(clr);
	p->drawLine(line_up);
	p->drawLine(line_down);
	p->drawRect(rc);
}
static QColor getBarColor(KColorStyle*style, int pos_open, int pos_close)
{
	// 位置越高，值越大
	if (pos_open < pos_close)
		return style->color(KColorStyle::KCOLOR_KLINE_GREEN);
	if (pos_open > pos_close)
		return style->color(KColorStyle::KCOLOR_KLINE_RED);

	return style->color(KColorStyle::KCOLOR_KLINE_WHITE);
}
void KLineLayer::draw(QPainter*p)
{
	if (!visible())
		return;
}

bool KLineLayer::contain(const QPointF&pt)
{
	return false;
}
int KLineLayer::dataLength()
{
	return 0;
}
void KLineLayer::setData(int type, QVector<QVariant>*_)
{}
QVector<QVariant>* KLineLayer::data(int type)
{
	return nullptr;
}
bool KLineLayer::getMaxMinValue(int start_idx, int count, double*max_v, double*min_v)
{
	return false;
}

