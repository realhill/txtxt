﻿#include "KIndicatorTrackingLayer.h"

#include "KLineCanvasContext.h"
#include "KLineViewContext.h"
#include "KColorStyle.h"
#include "KBarWidthStyle.h"

class KIndicatorTrackingLayer::KIndicatorTrackingLayerPrivate :public QObject
{
public:
	QVector<QVariant> open, close, high, low;
	KLineViewContext*view_context;
	int globalcolor_index;
	QColor line_color;
public:
	KIndicatorTrackingLayerPrivate(QObject*parent) :QObject(parent)
	{
		view_context = nullptr;
		globalcolor_index = -1;
	}
	void getValue(int idx, double*_open, double*_close, double*_high, double*_low)
	{
		*_open = open[idx].toDouble();
		*_close = close[idx].toDouble();
		*_high = high[idx].toDouble();
		*_low = low[idx].toDouble();
	}
};

KIndicatorTrackingLayer::KIndicatorTrackingLayer(QObject *parent)
	: KLayer(parent)
{
}

KIndicatorTrackingLayer::~KIndicatorTrackingLayer()
{
}

void KIndicatorTrackingLayer::setViewContext(KLineViewContext*_)
{
	private_ptr->view_context = _;
}
