﻿#pragma once

#include "KView.h"

class KLineCanvasContext;
class KLineMouseContext;
class KLineViewContext;
class KLineLayer;
class KLineView : public KView
{
	Q_OBJECT
private:
	class KLineViewPrivate;
	KLineViewPrivate*private_ptr;
public:
	KLineView(QObject *parent);
	virtual ~KLineView();

public:
	void setTrackingArea(const QRectF&l, const QRectF&r);
	void setCanvasContext(KLineCanvasContext*);
	KLineViewContext* context();
	bool selected();
	void setSelected(bool);
	int  dataLength();
	void getMaxMinValue(int start_idx, int count, QVariant*max_v, QVariant*min_v);
	int  getMaxItemCount();
public:
	KLineLayer* mainLayer();
	void setMainLayer(KLineLayer*);
	void addLayer(KLineLayer*);
	KLineLayer* layer(const QString&);
	void removeLayer(const QString&);
	int layerCount();
private:
	void drawViewSelected(QPainter*painter, QPaintEvent *event);
public:
	//virtual QRectF rect();
	virtual void setRect(const QRectF&);
	virtual void onCanvasDraging();
	virtual bool updateMouseContext(const QPointF&pt, KLineMouseContext*);
	virtual void onPaint(QPainter*p, QPaintEvent *event) override;

};
