﻿#include "ChartScene.h"

#include "ChartSceneStruct.h"

int createInstance(void**out)
{
	if (out) {
		ChartSceneStruct*st = new ChartSceneStruct(nullptr);
		*out = dynamic_cast<AstStruct*>(st);
	}
	return 0;
}
void freeInstance(void*_)
{
	if (_) {
		ChartSceneStruct*st = (ChartSceneStruct*)_;
		delete st;
	}
}