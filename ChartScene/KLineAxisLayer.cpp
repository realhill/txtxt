﻿#include "KLineAxisLayer.h"
#include <QPainter>
#include <QPen>
#include <QDateTime>

#include "KLineCanvasContext.h"
#include "KLineViewContext.h"
#include "KBarWidthStyle.h"
#include "KColorStyle.h"

class KLineAxisLayer::KLineAxisLayerPrivate :public QObject
{
public:
	QVector<QVariant> data;
public:
	KLineAxisLayerPrivate(QObject*parent) :QObject(parent)
	{
	}
};

KLineAxisLayer::KLineAxisLayer(QObject *parent)
	: KLineLayer(parent),private_ptr(new KLineAxisLayerPrivate(this))
{
	setColor(KColorStyle::KCOLOR_TITLE);
}

KLineAxisLayer::~KLineAxisLayer()
{
}

void KLineAxisLayer::setDate(QVector<QVariant>*_, const QString&fmt)
{
	if (_) {
		private_ptr->data = *_;
		QVector<QVariant>::iterator itr = private_ptr->data.begin();
		for (; itr != private_ptr->data.end(); itr++) {
			if (QVariant::String == itr->type()) {
				itr->setValue(QDateTime::fromString(itr->toString(), fmt));
			}
		}
	}
	else {
		private_ptr->data.clear();
	}
}

static QString v2t(const QVariant&v)
{
	switch (v.type()) {// (QVariant::DateTime == private_ptr->data[i].type())
	case QVariant::DateTime:
		return v.toDateTime().toString("yyyy-MM-dd hh:mm:ss ddd ");
		break;
	case QVariant::String:
	case QVariant::Double:
	case QVariant::Int:
		return v.toString();
		break;
	default:
		break;
	}
	return "";
}
void KLineAxisLayer::draw(QPainter*p)
{
	if (!visible())
		return;

	if (!private_ptr->data.count())
		return;

	KLineViewContext*view_context = viewContext();
	KLineCanvasContext*canvas_ctx = view_context->canvas_context;
	if (0 == canvas_ctx->canvas_colcount)
		return;


	int n = private_ptr->data.count() - canvas_ctx->item_startidx;
	if (n <= 0)
		return;

	int endidx = private_ptr->data.count() - 1;
	if (canvas_ctx->item_startidx + canvas_ctx->canvas_colcount - 1 <= endidx)
		endidx = canvas_ctx->item_startidx + canvas_ctx->canvas_colcount - 1;

	QPoint pt;
	QPolygon polygon;

	QRectF rc = view_context->rt_view;//this->rect();//canvas_ctx->rect_firstcol;// .intersected(private_ptr->view_context->rt_view);
	int baritem_center = KBarWidthStyle::item(canvas_ctx->barwidthstyle_idx)->center();

	QString dt;// = KUtils::kdatetimeOf(item->date, item->time).toString("yyyy-MM-dd hh:mm:ss ddd ");

	int w = p->fontMetrics().horizontalAdvance(QString("%1").arg(dt));

	p->save();

	p->setPen(color());

	for (int i = canvas_ctx->item_startidx; i <= endidx; i++) {
		if (canvas_ctx->item_startidx == i) {
			dt = v2t(private_ptr->data[i]);
			w = p->fontMetrics().horizontalAdvance(dt);
			rc.setWidth(w);

			p->drawText(rc, Qt::AlignVCenter | Qt::AlignLeft, dt);
		}


		//if (i != canvas_ctx->item_startidx)
		//	rc.translate(canvas_ctx->canvas_colwidth, 0);
		//pt.setX(rc.left() + baritem_center);
		//pt.setY(view_context->posYAtValue(private_ptr->data[i].toDouble()));
		//polygon.append(pt);
	}

	p->restore();
}

bool KLineAxisLayer::contain(const QPointF&pt)
{
	return false;
}
int KLineAxisLayer::dataLength()
{
	return private_ptr->data.count();
}
void KLineAxisLayer::setData(int type, QVector<QVariant>*_)
{
	if (_)
		private_ptr->data = *_;
	else
		private_ptr->data.clear();
}
QVector<QVariant>* KLineAxisLayer::data(int type)
{
	return &private_ptr->data;
}
bool KLineAxisLayer::getMaxMinValue(int start_idx, int count, double*max_v, double*min_v)
{
	return false;
}

