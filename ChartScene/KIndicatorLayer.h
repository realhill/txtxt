﻿#pragma once
#include <QVector>
#include <QVariant>

#include "KLineLayer.h"

class KLineViewContext;
class KIndicatorLayer : public KLineLayer
{
	Q_OBJECT
private:
	class KIndicatorLayerPrivate;
	KIndicatorLayerPrivate*private_ptr;

public:
	KIndicatorLayer(QObject *parent);
	~KIndicatorLayer();

	//void setData(QVector<QVariant>*);

	virtual void draw(QPainter*p) override;
	virtual bool contain(const QPointF&) override;
	virtual void setData(int type, QVector<QVariant>*) override;
	virtual QVector<QVariant>* data(int type) override;
	virtual int dataLength() override;
	virtual bool getMaxMinValue(int start_idx, int count, double*max_v, double*min_v) override;

};
