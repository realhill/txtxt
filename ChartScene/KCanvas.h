﻿#pragma once

#include <QObject>
#include <QWidget>

class KCanvas : public QObject
{
	Q_OBJECT
private:
	class KCanvasPrivate;
	KCanvasPrivate*private_ptr;

public:
	KCanvas(QObject*parent);
	virtual ~KCanvas();
public:
	void attachWidget(QWidget*, bool resize = true);
	QWidget*widget();
	QRectF rect();

	void update(QRectF rc);
	void update();
	void setCursor(const QCursor&cur);

protected:
	virtual QColor backgroundColor();
	virtual void onWidgetAttach(QWidget*old, QWidget*wnd);
	// 窗体消息
	virtual void onMouseMove(QMouseEvent *);
	virtual void onMousePress(QMouseEvent *);
	virtual void onMouseRelease(QMouseEvent *);
	virtual void onResize();
	virtual void onWheel(int delta, QWheelEvent *ev);
	virtual void onKeyPress(QKeyEvent*);
	virtual void onKeyRelease(QKeyEvent*);
	virtual void onEnter(QEvent *);
	virtual void onLeave(QEvent *);
	virtual void onPaint(QPainter*, QPaintEvent*);
	//virtual void onWidgetPainted(QPainter*, QPaintEvent*);
private:
	virtual bool eventFilter(QObject *watched, QEvent *event) override;
protected:
	virtual void paintEvent(QPaintEvent *); //重绘事件处理函数的声明: 所有的绘制操作都要在这个函数里面完成。
	virtual void mousePressEvent(QMouseEvent *);
	virtual void mouseDblClickEvent(QMouseEvent *);
	virtual void mouseMoveEvent(QMouseEvent *);
	virtual void wheelEvent(QWheelEvent *);
	virtual void resizeEvent(QResizeEvent *);
	virtual void mouseReleaseEvent(QMouseEvent*);
	virtual void enterEvent(QEvent *);
	virtual void leaveEvent(QEvent *);
	virtual void keyPressEvent(QKeyEvent*);
	virtual void keyReleaseEvent(QKeyEvent *event);
};
