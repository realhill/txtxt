﻿#pragma once

#include "KLineLayer.h"

class KLineAxisLayer : public KLineLayer
{
	Q_OBJECT
private:
	class KLineAxisLayerPrivate;
	KLineAxisLayerPrivate*private_ptr;
public:
	KLineAxisLayer(QObject *parent);
	~KLineAxisLayer();

	void setDate(QVector<QVariant>*, const QString&fmt);

	virtual void draw(QPainter*p) override;
	virtual bool contain(const QPointF&) override;
	virtual void setData(int type, QVector<QVariant>*) override;
	virtual QVector<QVariant>* data(int type) override;
	virtual int dataLength() override;
	virtual bool getMaxMinValue(int start_idx, int count, double*max_v, double*min_v) override;
};
