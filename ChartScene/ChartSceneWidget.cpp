﻿#include "ChartSceneWidget.h"
#include "ui_ChartSceneWidget.h"

#include "GlobalDefine.h"

#include "KLineCanvas.h"

class ChartSceneWidget::ChartSceneWidgetPrivate :public QObject, public AstScene
{
public:
	QString label_name;
	KLineCanvas * canvas;
public:
	ChartSceneWidgetPrivate(QObject*parent) :QObject(parent)
	{
		canvas = new KLineCanvas(this);
		canvas->attachWidget((QWidget*)parent);
		canvas->setMargin(100, 100, 20, 40);
		canvas->setBorderVisible(1, 0, 0, 1);
	}

	virtual int type() override
	{
		return AstScene::SCENE_OUT;
	}
	virtual QString typeName() override
	{
		return "ChartScene";
	}
	virtual QString label()
	{
		return label_name;
	}
	virtual void setLabel(const QString&s)
	{
		label_name = s;
	}
	virtual QWidget* widget() override
	{
		return qobject_cast<QWidget*>(parent());
	}
	virtual void onOpening() override
	{}
	virtual void onClosing() override
	{}

};


ChartSceneWidget::ChartSceneWidget(QWidget *parent)
	: QWidget(parent),private_ptr(new ChartSceneWidgetPrivate(this))
{
	ui = new Ui::ChartSceneWidget();
	ui->setupUi(this);
}

ChartSceneWidget::~ChartSceneWidget()
{
	delete ui;
}
AstScene* ChartSceneWidget::operator()()
{
	return dynamic_cast<AstScene*>(private_ptr);
}
