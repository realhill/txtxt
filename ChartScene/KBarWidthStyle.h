﻿#pragma once

#include <QObject>

class KBarWidthStyleItem 
{
public:
	int inner_width;  // 内部宽度
	int left_space;	// 内部空间
	int right_space;	// 内部空间
	int width()
	{
		return inner_width + left_space + right_space;
	}
	int center()
	{
		return left_space + inner_width / 2 + 0;
	}
};

class KBarWidthStyle
{
public:
	//static int idx(int w, int s);
	static KBarWidthStyleItem* item(int idx);
	static KBarWidthStyleItem* nearItem(int idx);
	static int defaultIdx();
	static KBarWidthStyleItem* defaultItem();
	static int count();
};
