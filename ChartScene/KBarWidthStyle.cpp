﻿#include "KBarWidthStyle.h"


static KBarWidthStyleItem default_styles[] = {
	// 0      1       2       3       4       5       6       7       8       9
	// ------------------------------------------------------------------------------
	{1, 0, 0},	{1, 1, 0},	{3,1,0},	{5, 1, 0},	{7, 1, 0},
	{9,1,0},	{11, 1, 1},	{13, 1, 1},	{15, 1, 1},	{17,1, 1},
	{19, 1, 1},	{21,2, 1},	{23, 2, 1},	{25, 2, 1},	{27, 2, 1},
	{29, 2, 1},	{31, 2, 2},	{33, 2, 2},	{35, 2, 2},	{37, 2, 2},
	{39,2, 2},	{41, 2, 2},	{43, 2, 2},	{45, 2, 2},	{47, 2, 2},
};

KBarWidthStyleItem* KBarWidthStyle::item(int idx)
{
	if (idx < 0 || idx >= count())
		return nullptr;

	return &default_styles[idx];
}
KBarWidthStyleItem* KBarWidthStyle::nearItem(int idx)
{
	if (idx < 0)
		idx = 0;
	if (idx >= count())
		idx = count() - 1;

	return &default_styles[idx];
}
int KBarWidthStyle::defaultIdx()
{
	return 12;
}
KBarWidthStyleItem* KBarWidthStyle::defaultItem()
{
	return item(defaultIdx());
}
int KBarWidthStyle::count()
{
	return sizeof(default_styles) / sizeof(KBarWidthStyleItem);
}
