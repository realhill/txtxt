﻿#pragma once

#include <QDateTime>
#include <QMap>
#include <QList>
#include <QVector>
#include <QColor>

enum enumViewArea{
	VIEWAREA_NONE = 0,
	VIEWAREA_AXIS,							// 坐标轴
	VIEWAREA_HEAD,
	VIEWAREA_VIEW,
	VIEWAREA_DATA,
	VIEWAREA_TAIL,
	VIEWAREA_TRACKING,
	VIEWAREA_INDICATOR_GROUP_SELECTOR,		// 指标组
	VIEWAREA_INDICATOR_GROUP,				// 指标组
	VIEWAREA_INDICATOR,						// 指标
	VIEWAREA_BOTTOM,
	VIEWAREA_TOP,
	VIEWAREA_LEFT,
	VIEWAREA_RIGHT,
	VIEWAREA_INDICATOR_DATA,				// 指标数据图表线上
	VIEWAREA_GRAPH_POINT,					// 绘图点
	VIEWAREA_GRAPH_BODY,					// 绘图
	VIEWAREA_DATE,
	VIEWAREA_SPLITTER,						// 拖动条
	VIEWAREA_TRADEDOT,						// 交易点（多开，多平，空开，空平）
	VIEWAREA_END
};

static Qt::CursorShape mapToCursor(int t)
{
	//Qt::CursorShape::ArrowCursor;
	Qt::CursorShape cur = Qt::ArrowCursor;
	switch (t) {
	case VIEWAREA_HEAD:
	case VIEWAREA_VIEW:
	case VIEWAREA_DATA:
	case VIEWAREA_TAIL:
	case VIEWAREA_TRACKING:
		break;
	case VIEWAREA_INDICATOR_GROUP_SELECTOR:		// 指标组
	case VIEWAREA_INDICATOR_GROUP:				// 指标组
	case VIEWAREA_INDICATOR_DATA:
		cur = Qt::PointingHandCursor;
		break;
	//case VIEWAREA_TRADEDOT:
	//	cur = Qt::BlankCursor;
	//	break;
	case VIEWAREA_INDICATOR:					// 指标
		break;
	case VIEWAREA_SPLITTER:
		cur = Qt::SplitVCursor;
		break;
	case VIEWAREA_TOP:
		cur = Qt::SplitVCursor;
		break;
	case VIEWAREA_BOTTOM:
		cur = Qt::SplitVCursor;
		break;
	case VIEWAREA_LEFT:
		break;
	case VIEWAREA_RIGHT:
		break;
	}
	return cur;
}
