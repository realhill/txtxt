﻿#pragma once

#include <QObject>
#include "GlobalDefine.h"

class KLineAddLayerFunction : public QObject, public AstFunction
{
	Q_OBJECT
private:
	class KLineAddLayerFunctionPrivate;
	KLineAddLayerFunctionPrivate*private_ptr;

public:
	KLineAddLayerFunction(QObject *parent);
	~KLineAddLayerFunction();

public:
	AstFunction* operator()();
	// AstFunction

	static QString ID();
public:
	virtual QString name() override;
	virtual int* outputTypes() override;
	virtual int outputCount() override;
	virtual int* inputTypes() override;
	virtual int inputCount() override;
	virtual void* function() override;

};
