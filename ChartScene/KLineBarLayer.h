﻿#pragma once

#include <QObject>
#include <QVector>
#include <QVariant>

#include "KLineLayer.h"

class KLineViewContext;
class KLineBarLayer : public KLineLayer
{
	Q_OBJECT
private:
	class KLineBarLayerPrivate;
	KLineBarLayerPrivate*private_ptr;
public:
	enum {
		OPEN = 0,
		CLOSE,
		HIGH,
		LOW,
	};
public:
	KLineBarLayer(QObject *parent);
	virtual ~KLineBarLayer();


	//void setViewContext(KLineViewContext*);
	////void setData(QVector<QVariant>*open, QVector<QVariant>*close, QVector<QVariant>*high, QVector<QVariant>*low);

	//void setColor(int global_idx);
	//void setColor(const QColor&clr);
	//virtual QColor color() override;
	//void drawBar(QPainter*p, int idx);
	virtual void draw(QPainter*p) override;
	virtual bool contain(const QPointF&) override;
	virtual void setData(int type, QVector<QVariant>*) override;
	virtual QVector<QVariant>* data(int type) override;
	virtual int dataLength() override;
	virtual bool getMaxMinValue(int start_idx, int count, double*max_v, double*min_v) override;
};
