﻿#include "KCanvas.h"

#include <QPainter>
#include <QPaintEvent>

#define MAX_COLUMN_WIDTH	39//41	//10.0
#define MIN_VIEW_HEIGHT		20

class KCanvas::KCanvasPrivate : public QObject
{
public:
	KCanvasPrivate(QObject*parent) : QObject(parent)
	{
		widget = nullptr;
		pixmap = nullptr;
	}
	virtual ~KCanvasPrivate()
	{	}
public:
	QWidget								* widget;
	// 绘制变量
	QPixmap								* pixmap;						// 临时绘制
};
KCanvas::KCanvas(QObject*parent) : QObject(parent), private_ptr(new KCanvasPrivate(this))
{
}

KCanvas::~KCanvas()
{
}

void KCanvas::attachWidget(QWidget*wnd, bool resize)
{
	QWidget*old_wnd = private_ptr->widget;

	if (old_wnd) {
		old_wnd->removeEventFilter(this);
		private_ptr->widget = nullptr;
	}

	//初始化矩形的尺寸和放大缩小的系数
	if (wnd) {
		private_ptr->pixmap = new QPixmap(wnd->size());

		wnd->setMouseTracking(true);
		wnd->setAttribute(Qt::WA_NoBackground, true);
		wnd->setAutoFillBackground(false);
		wnd->setFocusPolicy(Qt::FocusPolicy::ClickFocus);

		wnd->installEventFilter(this);

		private_ptr->widget = wnd;

	}

	onWidgetAttach(old_wnd, wnd);
	if (wnd && resize)
		wnd->resize(wnd->size());
}

QWidget*KCanvas::widget()
{
	return private_ptr->widget;
}

QRectF KCanvas::rect()
{
	if (private_ptr->widget)
		return private_ptr->widget->rect();
	return QRectF();
}
void KCanvas::update(QRectF rc)
{
	if (!private_ptr->widget)
		return;
	private_ptr->widget->update(rc.x(), rc.y(), rc.width(), rc.height());
}
void KCanvas::update()
{
	if (!private_ptr->widget)
		return;
	private_ptr->widget->update();
}
void KCanvas::setCursor(const QCursor&cur)
{
	if (private_ptr->widget)
		private_ptr->widget->setCursor(cur);
}

QColor KCanvas::backgroundColor()
{
	return Qt::GlobalColor::black;
}
void KCanvas::onWidgetAttach(QWidget*old, QWidget*wnd)
{
}
void KCanvas::onMouseMove(QMouseEvent * ev)
{
}
void KCanvas::onMousePress(QMouseEvent * ev)
{}
void KCanvas::onMouseRelease(QMouseEvent *)
{}
void KCanvas::onResize()
{}
void KCanvas::onWheel(int delta, QWheelEvent *ev)
{
}
void KCanvas::onKeyPress(QKeyEvent*event)
{
}
void KCanvas::onKeyRelease(QKeyEvent*)
{}
void KCanvas::onEnter(QEvent *)
{}
void KCanvas::onLeave(QEvent *)
{}
void KCanvas::onPaint(QPainter*painter, QPaintEvent*event)
{}
bool KCanvas::eventFilter(QObject *watched, QEvent *event)
{
	if (watched == private_ptr->widget) {
		//event->accept();

		if (event->type() == QEvent::Wheel) {
			event->accept();
			wheelEvent(static_cast<QWheelEvent*>(event));
			return true;
		}
		if (event->type() == QEvent::MouseMove) {
			event->accept();
			mouseMoveEvent(static_cast<QMouseEvent*>(event));
			return true;
		}
		if (event->type() == QEvent::MouseButtonDblClick) {
			event->accept();
			mouseDblClickEvent(static_cast<QMouseEvent*>(event));
			return true;
		}
		if (event->type() == QEvent::MouseButtonPress) {
			event->accept();
			mousePressEvent(static_cast<QMouseEvent*>(event));
			return true;
		}
		if (event->type() == QEvent::MouseButtonRelease) {
			event->accept();
			mouseReleaseEvent(static_cast<QMouseEvent*>(event));
			return true;
		}
		if (event->type() == QEvent::KeyPress) {
			event->accept();
			keyPressEvent(static_cast<QKeyEvent*>(event));
			return true;
		}
		if (event->type() == QEvent::KeyRelease) {
			event->accept();
			keyReleaseEvent(static_cast<QKeyEvent*>(event));
			return true;
		}
		if (event->type() == QEvent::Enter) {
			event->accept();
			enterEvent(static_cast<QEvent*>(event));
			return true;
		}
		if (event->type() == QEvent::Leave) {
			event->accept();
			leaveEvent(static_cast<QEvent*>(event));
			return true;
		}
		if (event->type() == QEvent::Paint) {
			event->accept();
			paintEvent(static_cast<QPaintEvent*>(event));
			return true;
		}
		if (event->type() == QEvent::Resize) {
			event->accept();
			resizeEvent(static_cast<QResizeEvent*>(event));
			return true;
		}

		return false;
	}

	return QObject::eventFilter(watched, event);
}
//绘图
void KCanvas::paintEvent(QPaintEvent *event)
{
	QPixmap*pix = private_ptr->pixmap;
	QPainter p(pix);  //在画布上常见一个画家
	p.setClipRegion(event->region());
	do {
		QRegion::const_iterator itr = event->region().begin();
		for (; itr != event->region().end(); itr++)
			p.fillRect(*itr, backgroundColor());// context()->colorStyle()->color(KColorStyle::KCOLOR_BACKGROUND));
	} while (0);

	onPaint(&p, event);

	p.end();  //画完了
	/// 复制
	p.begin(widget());
	p.setClipRegion(event->region());
	do {
		QRegion::const_iterator itr = event->region().begin();
		for (; itr != event->region().end(); itr++)
			p.drawPixmap(*itr, *pix, *itr);
	} while (0);

}
//鼠标按键按下事件
void KCanvas::mousePressEvent(QMouseEvent *ev)
{
	onMousePress(ev);
}
//鼠标双击事件
void KCanvas::mouseDblClickEvent(QMouseEvent *ev)
{}
//鼠标滚轮事件
void KCanvas::wheelEvent(QWheelEvent *ev)
{
	int delta = 0;

	// 主图上滚动鼠标，缩放尺寸
	if (1) {
		if (ev->delta() > 0)
			delta = 1;
		else//如果滚轮向下滚动就缩小尺寸
			delta = -1;
		//如果滚轮向上滚动就放大尺寸
		if (!!delta)
			onWheel(delta, ev);
			//zoom(delta, !!(ev->buttons() & Qt::LeftButton));
	}
}
//鼠标移动事件
void KCanvas::mouseMoveEvent(QMouseEvent *ev)
{
	onMouseMove(ev);
}
void KCanvas::resizeEvent(QResizeEvent *ev)
{
	if (private_ptr->pixmap)
		delete private_ptr->pixmap;
	private_ptr->pixmap = new QPixmap(ev->size());

	onResize();
}
void KCanvas::mouseReleaseEvent(QMouseEvent*ev)
{
	onMouseRelease(ev);
}
void KCanvas::enterEvent(QEvent *event)
{
	onEnter(event);
}
void KCanvas::leaveEvent(QEvent *event)
{
	onLeave(event);
}
void KCanvas::keyPressEvent(QKeyEvent*event)
{
	onKeyPress(event);
}
void KCanvas::keyReleaseEvent(QKeyEvent *event)
{
	onKeyRelease(event);
}

