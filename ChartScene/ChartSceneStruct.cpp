﻿#include "ChartSceneStruct.h"
#include "ChartSceneWidget.h"
#include "KLineSceneWidget.h"

//#define CHART_SCENE_NAME "ChartOutScene"
#define KLINE_SCENE_NAME "KLineOutScene"

#include "KLineAddLayerFunction.h"
#include "KLineAddBarFunction.h"
#include "KLineSetAxisFunction.h"

class ChartSceneStruct::ChartSceneStructPrivate :public QObject
{
public:
	KLineAddLayerFunction * addlayer_function;
	KLineAddBarFunction * addbar_function;
	KLineSetAxisFunction*setaxis_function;

public:
	ChartSceneStructPrivate(QObject*parent) :QObject(parent)
	{
		addlayer_function = new KLineAddLayerFunction(this);
		addbar_function = new KLineAddBarFunction(this);
		setaxis_function = new KLineSetAxisFunction(this);
	}
};

ChartSceneStruct::ChartSceneStruct(QObject *parent)
	: QObject(parent), private_ptr(new ChartSceneStructPrivate(this))
{
}

ChartSceneStruct::~ChartSceneStruct()
{
}

//////////////////////////////////////////////////////
//
QString ChartSceneStruct::sceneNames(int scene_type)
{
	if (AstScene::SCENE_OUT == scene_type)
		return KLINE_SCENE_NAME;

	return "";
}
bool ChartSceneStruct::hasScene(const QString&name, int scene_type)
{
	switch (scene_type) {
	case AstScene::SCENE_OUT:
		//if (0 == name.compare(CHART_SCENE_NAME, Qt::CaseInsensitive))
		//	return true;
		if (0 == name.compare(KLINE_SCENE_NAME, Qt::CaseInsensitive))
			return true;
		break;
	}

	return false;
}
AstScene* ChartSceneStruct::createScene(const QString&name, int scene_type, AstStructContext*ctx)
{
	switch (scene_type) {
	case AstScene::SCENE_OUT:
		//if (0 == name.compare(CHART_SCENE_NAME, Qt::CaseInsensitive)) {
		//	ChartSceneWidget*wgt = new ChartSceneWidget(nullptr);
		//	return wgt ? wgt->operator()() : nullptr;
		//}
		if (0 == name.compare(KLINE_SCENE_NAME, Qt::CaseInsensitive)) {
			KLineSceneWidget*wgt = new KLineSceneWidget(nullptr);
			return wgt ? wgt->operator()() : nullptr;
		}
		break;
	}
	return nullptr;
}
void ChartSceneStruct::freeScene(AstScene*_)
{
	delete _->widget();
}
QString ChartSceneStruct::functionNames()
{
	return "AddLayer,AddBar,SetAxis";// "TextOutScenePrint";
}
AstFunction* ChartSceneStruct::getFunction(const QString&name)
{
	if (0 == name.compare("AddLayer", Qt::CaseSensitivity::CaseInsensitive)) {
		return private_ptr->addlayer_function->operator()();
	}
	if (0 == name.compare("AddBar", Qt::CaseSensitivity::CaseInsensitive)) {
		return private_ptr->addbar_function->operator()();
	}
	if (0 == name.compare("SetAxis", Qt::CaseSensitivity::CaseInsensitive)) {
		return private_ptr->setaxis_function->operator()();
	}
	return nullptr;
}
