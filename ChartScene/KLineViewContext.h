﻿#pragma once

#include <QObject>

class KLineCanvasContext;

class KLineViewContext
{
public:
	KLineCanvasContext			* canvas_context;
	double						max_value;
	double						min_value;
	QRectF						rt_view;
public:
	KLineViewContext()
	{
		canvas_context = nullptr;
		max_value = 0;
		min_value = 0;
	}
	int posYAtValue(double v)
	{
		if ((int)rt_view.height() == 0)
			return rt_view.top();

		double delta = max_value - min_value;

		if (!delta)
			return rt_view.center().y();

		return rt_view.bottom() - (v - min_value) * rt_view.height() / delta;
	}
};
