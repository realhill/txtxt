﻿#pragma once

#include <QObject>
#include <QRect>

class QPainter;
class QPaintEvent;
//class KMouseContext;
class KView : public QObject
{
	Q_OBJECT
private:
	class KViewPrivate;
	KViewPrivate*private_ptr;
public:
	KView(QObject *parent);
	virtual ~KView();
public:
	int id();
	void setId(int);
	QString name();
	void setName(const QString&);
	virtual QRectF rect();
	virtual void setRect(const QRectF&);

	void draw(QPainter*p, QPaintEvent *event);
protected:
	virtual void onPaint(QPainter*p, QPaintEvent *event);
};
