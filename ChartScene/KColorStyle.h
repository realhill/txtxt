﻿#pragma once

#include <QObject>
#include <QColor>
#include <QVector>

class KColorStyle : public QObject
{
	Q_OBJECT

private:
	class KColorStylePrivate;
	KColorStylePrivate*private_ptr;
public:
	enum {
		KCOLOR_DEFAULT = 0,
		KCOLOR_BACKGROUND,			// 窗口底色
		KCOLOR_TITLE,				// 窗口标题
		KCOLOR_TEXT,				// 字符显示色
		KCOLOR_WARN,				// 提醒色
		KCOLOR_AXIS,				// 坐标线
		KCOLOR_AXIS_SCALE,			// 坐标刻度
		KCOLOR_BORDER,				// 边框
		KCOLOR_KLINE_RED,			// K线阳线
		KCOLOR_KLINE_GREEN,			// K线阴线
		KCOLOR_KLINE_WHITE,		// K线平盘
		KCOLOR_INDICATOR_NAME,		// 指标名
		KCOLOR_INDICATOR_LINE1,		// 指标线第一条
		KCOLOR_INDICATOR_LINE2,		// 指标线第一条
		KCOLOR_INDICATOR_LINE3,		// 指标线第一条
		KCOLOR_INDICATOR_LINE4,		// 指标线第一条
		KCOLOR_INDICATOR_LINE5,		// 指标线第一条
		KCOLOR_INDICATOR_LINE6,		// 指标线第一条
		KCOLOR_INDICATOR_LINE7,		// 指标线第一条
		KCOLOR_INDICATOR_LINE8,		// 指标线第一条
		KCOLOR_INDICATOR_LINE9,		// 指标线第一条
		KCOLOR_INDICATOR_LINE10,	// 指标线第一条
		KCOLOR_MAX
	};
public:
	KColorStyle(QObject *parent);
	~KColorStyle();
protected:
	void init();
	void setGlobalColor(QVector<QColor> * arr, int idx, const QColor& clr);
	QColor globalColor(const QVector<QColor> * arr, int idx);
public:
	QColor color(int idx);
	void setColor(int idx, QColor clr);
	void selectStyle(const QString&style_name);
	void changeStyle();
};

