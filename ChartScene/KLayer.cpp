﻿#include "KLayer.h"

#include "KColorStyle.h"

class KLayer::KLayerPrivate :public QObject
{
public:
	QRectF rect;
	QString name;
	bool visible;
	int type;
public:
	KLayerPrivate(QObject*parent) :QObject(parent)
	{
		visible = true;
		type = KLayer::LAYER_TYPE_NONE;
	}
};

KLayer::KLayer(QObject *parent):QObject(parent),private_ptr(new KLayerPrivate(this))
{}

KLayer::~KLayer()
{}

QString KLayer::name()
{
	return private_ptr->name;
}
void KLayer::setName(const QString&_)
{
	private_ptr->name = _;
}
int KLayer::type()
{
	return private_ptr->type;
}
void KLayer::setType(int t)
{
	private_ptr->type = t;
}
QRectF KLayer::rect()
{
	return private_ptr->rect;
}
void KLayer::setRect(const QRectF&_)
{
	private_ptr->rect = _;
}
//QColor KLayer::color()
//{
//	return QColor();
//}
bool KLayer::visible()
{
	return private_ptr->visible;
}
void KLayer::setVisible(bool b)
{
	private_ptr->visible = b;
}
//void KLayer::draw(QPainter*p)
//{}
//bool KLayer::contain(const QPointF&)
//{
//	return false;
//}
//void KLayer::setData(int type, QVector<QVariant>*)
//{}
//QVector<QVariant>* KLayer::data(int type)
//{
//	return nullptr;
//}
//int KLayer::dataLength()
//{
//	return 0;
//}
//bool KLayer::getMaxMinValue(int start_idx, int count, double*max_v, double*min_v)
//{
//	return false;
//}
