﻿#pragma once

#include <QObject>
#include <QVector>
#include <QVariant>

#include "KLayer.h"

class KLineViewContext;
class KLineLayer : public KLayer
{
	Q_OBJECT
private:
	class KLineLayerPrivate;
	KLineLayerPrivate*private_ptr;
public:
	KLineLayer(QObject *parent);
	virtual ~KLineLayer();

	KLineViewContext*viewContext();
	void setViewContext(KLineViewContext*);
	void setColor(int global_idx);
	void setColor(const QColor&clr);
	virtual void draw(QPainter*p) override;
	virtual QColor color();
	virtual bool contain(const QPointF&);
	virtual void setData(int type, QVector<QVariant>*);
	virtual QVector<QVariant>* data(int type);
	virtual int dataLength();
	virtual bool getMaxMinValue(int start_idx, int count, double*max_v, double*min_v);


};
