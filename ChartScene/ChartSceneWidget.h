﻿#pragma once

#include <QWidget>
namespace Ui { class ChartSceneWidget; };

class AstScene;
class ChartSceneWidget : public QWidget
{
	Q_OBJECT

private:
	class ChartSceneWidgetPrivate;
	ChartSceneWidgetPrivate*private_ptr;
public:
	ChartSceneWidget(QWidget *parent = Q_NULLPTR);
	~ChartSceneWidget();

	AstScene* operator()();
private:
	Ui::ChartSceneWidget *ui;
};
