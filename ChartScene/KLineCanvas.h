﻿#pragma once

#include "KCanvas.h"

class KLineView;
class KLineAxisView;
class KColorStyle;
class KLineCanvasContext;
class KLineMouseContext;
class KLineCanvas : public KCanvas
{
	Q_OBJECT
private:
	class KLineCanvasPrivate;
	KLineCanvasPrivate*private_ptr;
public:
	KLineCanvas(QObject *parent);
	virtual ~KLineCanvas();

	KColorStyle* colorStyle();
	KLineCanvasContext*context();
	void recalc();

protected:
	void drawTrackingLine(QPainter*painter);
	void onZoom(int/*, bool wheel_enable, Qt::KeyboardModifiers*/);
	void onColTranslation(int delta);

public:
	virtual bool updateMouseContext(const QPoint&pt, KLineMouseContext*);

protected:
	virtual QColor backgroundColor() override; //重绘事件处理函数的声明: 所有的绘制操作都要在这个函数里面完成。
	virtual void onMouseMove(QMouseEvent *) override;
	virtual void onMousePress(QMouseEvent *) override;
	virtual void onMouseRelease(QMouseEvent *)override;
	virtual void onKeyPress(QKeyEvent*event) override;
	virtual void onEnter(QEvent *) override;
	virtual void onLeave(QEvent *) override;
	virtual void onResize() override;
	virtual void onWheel(int delta, QWheelEvent *ev) override;
	virtual void onPaint(QPainter*, QPaintEvent*) override;
protected:
	virtual void mousePressEvent(QMouseEvent *) override;
	virtual void mouseMoveEvent(QMouseEvent *) override;
	virtual void mouseReleaseEvent(QMouseEvent*) override;
public:
	void setMargin(int left_margin, int right_margin, int top_margin, int bottom_margin);
	void setBorderVisible(int left_enable, int right_enable, int top_enable, int bottom_enable);
	// 
	//int colCount();
	QRectF colRect(int);
	QRectF colRectOfPoint(const QPointF&);

	KLineAxisView* axisView();

	KLineView* addView(const QString&name/*, KLineView**/);
	bool delView(const QString&name);
	bool delView(int idx);
	void selectView(void*v);
	KLineView* view(int idx);
	KLineView* view(const QString&name);
	int viewCount();
	KLineView*curSelectedView();
};
