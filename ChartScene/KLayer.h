﻿#pragma once

#include <QObject>
#include <QRect>
#include <QVariant>
#include <QVector>

class QPainter;
class KLayer : public QObject
{
	Q_OBJECT
private:
	class KLayerPrivate;
	KLayerPrivate*private_ptr;
public:
	enum enumKLayerType
	{
		LAYER_TYPE_NONE = 0,
		LAYER_TYPE_KLINE,
		LAYER_TYPE_INDICATOR,
		LAYER_TYPE_GRAPH,
		LAYER_TYPE_REFLINE,		// 参考线
	};

public:
	KLayer(QObject *parent);
	virtual ~KLayer();

	QString name();
	void setName(const QString&);
	int type();
	void setType(int);
	QRectF rect();
	void setRect(const QRectF&);
	bool visible();
	void setVisible(bool);
	virtual void draw(QPainter*p) = 0;
};
