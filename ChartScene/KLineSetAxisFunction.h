﻿#pragma once

#include <QObject>
#include "GlobalDefine.h"

class KLineSetAxisFunction : public QObject, public AstFunction
{
	Q_OBJECT

public:
	KLineSetAxisFunction(QObject *parent);
	~KLineSetAxisFunction();
private:
	class KLineSetAxisFunctionPrivate;
	KLineSetAxisFunctionPrivate*private_ptr;

public:
	AstFunction* operator()();
	// AstFunction

	static QString ID();
public:
	virtual QString name() override;
	virtual int* outputTypes() override;
	virtual int outputCount() override;
	virtual int* inputTypes() override;
	virtual int inputCount() override;
	virtual void* function() override;

};
