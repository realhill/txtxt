﻿#pragma once

#include "KLayer.h"

class KLineViewContext;
class KIndicatorTrackingLayer : public KLayer
{
	Q_OBJECT
private:
	class KIndicatorTrackingLayerPrivate;
	KIndicatorTrackingLayerPrivate*private_ptr;

public:
	KIndicatorTrackingLayer(QObject *parent);
	~KIndicatorTrackingLayer();

	void setViewContext(KLineViewContext*);

};
