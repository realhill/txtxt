﻿#pragma once

#include <QWidget>
namespace Ui { class KLineSceneWidget; };

class KLineCanvas;
class AstScene;
class KLineSceneWidget : public QWidget
{
	Q_OBJECT

private:
	class KLineSceneWidgetPrivate;
	KLineSceneWidgetPrivate*private_ptr;
public:
	KLineSceneWidget(QWidget *parent = Q_NULLPTR);
	~KLineSceneWidget();

	AstScene* operator()();

	KLineCanvas*canvas();
private:
	Ui::KLineSceneWidget *ui;
};
