#ifndef _VARIABLE_H
#define _VARIABLE_H
#ifdef __cplusplus
extern "C"
{
#endif

#include "variant.h"

/* 符号表 */
struct variable_s {
    char *name;
    struct variant_s value;
};

int variable_create(struct variable_s**);
struct variable_s* variable_create_ex(const char*name, struct variant_s value);
void variable_set_value(struct variable_s*, struct variant_s value);
void variable_free(struct variable_s*);
void variable_clean(struct variable_s*);

#ifdef __cplusplus
}
#endif
#endif
