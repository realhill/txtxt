﻿#ifndef _AST_ERROR_H
#define _AST_ERROR_H
#ifdef __cplusplus
extern "C"
{
#endif

enum {
    ERR_MAX_NUMBER____              = -9999,
    ERR_PARAM_TOO_MUCH,             // 参数过多
    ERR_PARAM_NOT_ENOUGH,           // 参数不足
    ERR_PARAM_NOT_MATCH,            // 参数类型不匹配

    ERR_VARIABLE_NOT_DEFINED,       // 变量未定义
    ERR_DATATYPE_NOT_MATCH,         // 数据类型不匹配
	ERR_UNKNOWN = -1,
    ERR_OK = 0                      // ok
};

#ifdef __cplusplus
}
#endif
#endif
