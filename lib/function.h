﻿#ifndef _FUNCTION_H
#define _FUNCTION_H
#ifdef __cplusplus
extern "C"
{
#endif

//#include "variant.h"

struct variant_s;
// 外部扩展函数定义
typedef int (*AST_FUNCTION)(struct variant_s *in, int in_count, struct variant_s *out, void*tag);

// enum enum_function_param_type {
//     FPT_NONE    = 0,
//     FPT_VOID    = 0x0001,           // 主要用于输出定义
//     FPT_NULL    = 0x0002,           // NULL
//     FPT_NUMBER  = 0x0004,           // 数字
//     FPT_STRING  = 0x0008,           // 字符串
//     FPT_VECTOR  = 0x0010,           // 数组
//     FPT_POINTER = 0x0020,			// 指针
//     FPT_ALL     = 0xFFFF,           // 所有
// };
enum enum_function_param_type {
    FPT_NONE    = 0,
    FPT_VOID    = 0x0010,           // 主要用于输出定义
    FPT_NUMBER  = 0x0040,           // 数字
    FPT_STRING  = 0x0080,           // 字符串
    FPT_VECTOR  = 0x0100,           // 数组
    FPT_PTR     = 0x0200,			// 指针
    FPT_PPTR    = 0x0400,			// 二级指针
	FPT_ALL     = 0x0FF0,           // 所有
};

#define FPT_VECTOR_OR_NUMBER (FPT_NUMBER | FPT_VECTOR)
#define FPT_STRING_OR_NUMBER (FPT_NUMBER | FPT_STRING)

/* 符号表 */
struct function_s {
    char                * name;                     // 函数名
    int                 * out_param_types;          /* 返回数据类型定义 参看data_struct.h*/
    int                 out_param_count;
    int                 * in_param_types;           /* 函数参数数据类型定义  参看data_struct.h*/
    int                 in_param_count;
    AST_FUNCTION        func;                       /* 函数体 */
    void                * tag;
};

int function_create(struct function_s**);
struct function_s* function_create_ex(const char*name, AST_FUNCTION func, int* out_params, int out_param_count, int*in_params, int in_param_count, void*tag);
void function_free(struct function_s*);

#ifdef __cplusplus
}
#endif
#endif
