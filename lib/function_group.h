#ifndef _FUNCTION_GROUP_H
#define _FUNCTION_GROUP_H
#ifdef __cplusplus
extern "C"
{
#endif

#include "inner_define.h"

int function_group_create(function_group_t**);
int function_group_count(function_group_t*);
int function_group_append(function_group_t*, function_t*value);
int function_group_get_idx(function_group_t*, const char*key);
function_t* function_group_at(function_group_t*, int);
function_t* function_group_query(function_group_t*, const char*key);
function_t* function_group_remove(function_group_t*ptr, const char*key);
void function_group_free(function_group_t*);

#ifdef __cplusplus
}
#endif
#endif
