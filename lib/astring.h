#ifndef _ASTRING_H
#define _ASTRING_H
#ifdef __cplusplus
extern "C"
{
#endif

/* 字符串 */
struct astring_s {
    char                * value;
    int                 ref_count;
};

int  astring_create(struct astring_s**);
struct astring_s* astring_create_ex(const char*value);
void astring_free(struct astring_s*);
int  astring_addref(void*);
int  astring_release(void*);

#ifdef __cplusplus
}
#endif
#endif
