#ifndef _VARIABLE_GROUP_H
#define _VARIABLE_GROUP_H
#ifdef __cplusplus
extern "C"
{
#endif

#include "inner_define.h"

int variable_group_create(variable_group_t**);
int variable_group_count(variable_group_t*);
int variable_group_append(variable_group_t*, variable_t*value);
int variable_group_get_idx(variable_group_t*, const char*key);
variable_t* variable_group_at(variable_group_t*, int);
variable_t* variable_group_query(variable_group_t*, const char*key);
variable_t* variable_group_get(variable_group_t*, const char*key);
variable_t* variable_group_remove(variable_group_t*ptr, const char*key);
void variable_group_free(variable_group_t*);
void variable_group_clean(variable_group_t*);

#ifdef __cplusplus
}
#endif
#endif
