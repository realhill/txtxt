#ifndef _INNER_DEFINE_H
#define _INNER_DEFINE_H
#ifdef __cplusplus
extern "C"
{
#endif

typedef struct ast ast_t;
// typedef struct symlist_s symlist_t;
typedef struct symbol_s symbol_t;
// typedef struct symbol_group_s symbol_group_t;
typedef struct variable_s variable_t;
typedef struct variable_group_s variable_group_t;
typedef struct function_s function_t;
typedef struct function_group_s function_group_t;
typedef struct env_s env_t;
// typedef struct mini_program_s mini_program_t;
// typedef struct mini_program_group_s mini_program_group_t;

#ifdef __cplusplus
}
#endif
#endif
