#ifndef _DATA_STRUCT_H
#define _DATA_STRUCT_H
#ifdef __cplusplus
extern "C"
{
#endif

#define VALUETYPE_INT_BASE                  0x02        // integer
#define VALUETYPE_DOUBLE_BASE               0x04        // double

// 类型分类（函数参数用到）
#define VALUETYPE_NULL                      0x000        // NULL
#define VALUETYPE_VOID                      0x010        // void
#define VALUETYPE_NUMBER                    0x040        // 数分类
#define VALUETYPE_STRING                    0x080        // 字符串分类
#define VALUETYPE_ARRAY                     0x100        // 数组
#define VALUETYPE_PTR                       0x200        // 一级指针
#define VALUETYPE_PPTR                      0x400        // 二级指针
// 数分整型和小数
#define VALUETYPE_INTEGER                   (VALUETYPE_NUMBER | VALUETYPE_INT_BASE)        // integer
#define VALUETYPE_DOUBLE                    (VALUETYPE_NUMBER | VALUETYPE_DOUBLE_BASE)     // double
// 数组
#define VALUETYPE_STRING_VECTOR             (VALUETYPE_ARRAY | VALUETYPE_STRING)        // integer vector
#define VALUETYPE_INTEGER_VECTOR            (VALUETYPE_ARRAY | VALUETYPE_INTEGER)        // integer vector
#define VALUETYPE_DOUBLE_VECTOR             (VALUETYPE_ARRAY | VALUETYPE_DOUBLE)        // double vector
// 指针
#define VALUETYPE_STRING_PTR                (VALUETYPE_PTR | VALUETYPE_STRING)    // char*
#define VALUETYPE_VOID_PTR                  (VALUETYPE_PTR | VALUETYPE_VOID)    // void*
#define VALUETYPE_INTEGER_PTR               (VALUETYPE_PTR | VALUETYPE_INTEGER) // int*
#define VALUETYPE_DOUBLE_PTR                (VALUETYPE_PTR | VALUETYPE_DOUBLE)  // double*
// 数组的指针
#define VALUETYPE_INTEGER_VECTOR_PTR        (VALUETYPE_PTR | VALUETYPE_INTEGER_VECTOR) // int*
#define VALUETYPE_DOUBLE_VECTOR_PTR         (VALUETYPE_PTR | VALUETYPE_DOUBLE_VECTOR)  // double*
// 二级指针
#define VALUETYPE_VOID_PTRPTR               (VALUETYPE_PTRPTR | VALUETYPE_VOID) // void**

// /* 数据类型 */
// #define VALUETYPE_NULL                      0x00        // NULL
// #define VALUETYPE_VOID                      0x01        // void
// #define VALUETYPE_INTEGER                   0x02        // integer
// #define VALUETYPE_DOUBLE                    0x03        // double
// #define VALUETYPE_INTEGER_VECTOR            0x04        // integer vector
// #define VALUETYPE_DOUBLE_VECTOR             0x05        // double vector
// #define VALUETYPE_CHAR                      0x06        // char

// #define VALUETYPE_PTR                       0x0100        // 一级指针
// #define VALUETYPE_PPTR                      0x0200        // 二级指针

// #define VALUETYPE_CHAR_PTR                  (VALUETYPE_PTR | VALUETYPE_CHAR)    // char*
// #define VALUETYPE_VOID_PTR                  (VALUETYPE_PTR | VALUETYPE_VOID)    // void*
// #define VALUETYPE_INTEGER_PTR               (VALUETYPE_PTR | VALUETYPE_INTEGER) // int*
// #define VALUETYPE_DOUBLE_PTR                (VALUETYPE_PTR | VALUETYPE_DOUBLE)  // double*
// #define VALUETYPE_INTEGER_VECTOR_PTR        (VALUETYPE_PTR | VALUETYPE_INTEGER_VECTOR) // int*
// #define VALUETYPE_DOUBLE_VECTOR_PTR         (VALUETYPE_PTR | VALUETYPE_DOUBLE_VECTOR)  // double*

// #define VALUETYPE_VOID_PTRPTR               (VALUETYPE_PTRPTR | VALUETYPE_VOID) // void**


#ifdef __cplusplus
}
#endif
#endif
