#ifndef _ENV_H
#define _ENV_H
#ifdef __cplusplus
extern "C"
{
#endif

typedef struct env_errorlocation_s
{
    int first_line;
    int first_column;
    int last_line;
    int last_column;
} env_errorlocation;

typedef int (*ENV_OUTPUT_CALLBACK)(const char*name, struct variant_s*result, struct variant_s*extra_datas, int extra_count, void*cb_obj);

int  env_create(void**);

int  env_set_function_group(void*env, struct function_group_s*, int need_free);
struct function_group_s*  env_get_function_group(void*env);
struct function_s*  env_query_function(void*env, const char*);

int  env_set_variable_group(void*env, struct variable_group_s*);
struct variable_group_s*  env_get_variable_group(void*env);
struct variable_s*  env_query_variable(void*env, const char*);

int  env_set_constant_group(void*env, struct variable_group_s*);
struct variable_group_s*  env_get_constant_group(void*env);
struct variable_s*  env_query_constant(void*env, const char*);

void env_set_tag(void*env);
void* env_get_tag(void*env);
void env_set_output_callback(void*env, ENV_OUTPUT_CALLBACK cb, void *cb_obj);
void env_set_result(void*env, struct ast*);

void env_clean(void*env);
void env_free(void*env);

void env_set_errorstring(void*env, const char*s);
void env_set_errorstring_v(void*env, const char*s, va_list args);
void env_set_errorstring_p(void*env, const char*s, ...);
void env_clear_errorstring(void*env);
const char* env_get_errorstring(void*env);

void env_set_errorcode(void*env, int);
int  env_get_errorcode(void*env);

int  env_get_errorlocation(void*env, env_errorlocation*);

int  env_parse_string(void*env, const char*);
int  env_exec(void*env);


#ifdef __cplusplus
}
#endif
#endif
