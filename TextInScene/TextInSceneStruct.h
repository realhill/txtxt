﻿#pragma once

#include <QObject>

#include "GlobalDefine.h"

class TextInSceneStruct : public QObject,public AstStruct
{
	Q_OBJECT
private:
	class TextInSceneStructPrivate;
	TextInSceneStructPrivate*private_ptr;
public:
	TextInSceneStruct(QObject *parent);
	~TextInSceneStruct();

	virtual QString sceneNames(int scene_type) override;
	virtual bool hasScene(const QString&name, int scene_type) override;
	virtual AstScene* createScene(const QString&name, int scene_type, AstStructContext*ctx) override;
	virtual void freeScene(AstScene*) override;

	virtual QString functionNames() override;
	virtual AstFunction* getFunction(const QString&name) override;
	virtual QVector<AstConstant>* constants() override { return nullptr; }
};
