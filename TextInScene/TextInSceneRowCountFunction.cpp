﻿#include "TextInSceneRowCountFunction.h"

#include <QTextDocument>
#include <QTextBlock>

#include "TextInSceneWidget.h"

class TextInSceneRowCountFunction::TextInSceneRowCountFunctionPrivate :QObject
{
public:
	int in_params[2];
	int out_params[1];
	int in_count;
	int out_count;
public:
	TextInSceneRowCountFunctionPrivate(QObject*parent):QObject(parent)
	{
		in_count = 1;
		out_count = 1;

		in_params[0] = FPT_PTR;
		out_params[0] = FPT_NUMBER;
	}

};
TextInSceneRowCountFunction::TextInSceneRowCountFunction(QObject *parent)
	: QObject(parent),private_ptr(new TextInSceneRowCountFunctionPrivate(this))
{
}

TextInSceneRowCountFunction::~TextInSceneRowCountFunction()
{
}

static int TextInSceneRowCount_FUNCTION(struct variant_s *in, int in_count, struct variant_s *out, void*tag)
{
	if (1 != in_count)
		return ERR_PARAM_NOT_MATCH;

	if (VALUETYPE_VOID_PTR != in[0].type)
		return ERR_DATATYPE_NOT_MATCH;
	if (nullptr == in[0].u.ptr)
		return ERR_DATATYPE_NOT_MATCH;

	QTextDocument*doc = nullptr;

	try {
		AstScene*as = (AstScene*)in[0].u.ptr;
		TextInSceneWidget*ptr = dynamic_cast<TextInSceneWidget*>(as->widget());
		doc = ptr->textDocument();
	}
	catch (...) {
		doc = nullptr;
	}


	out->type = VALUETYPE_INTEGER;
	out->u.i = doc->blockCount();

	return 0;
}

AstFunction* TextInSceneRowCountFunction::operator()()
{
	return dynamic_cast<AstFunction*>(this);
}

QString TextInSceneRowCountFunction::name()
{
	return "TextInSceneRowCount";
}
int* TextInSceneRowCountFunction::outputTypes()
{
	return private_ptr->out_params;
}
int TextInSceneRowCountFunction::outputCount()
{
	return private_ptr->out_count;
}
int* TextInSceneRowCountFunction::inputTypes()
{
	return private_ptr->in_params;
}
int TextInSceneRowCountFunction::inputCount()
{
	return private_ptr->in_count;
}
void* TextInSceneRowCountFunction::function()
{
	return TextInSceneRowCount_FUNCTION;
}
