﻿#include "TextInScene.h"

#include "TextInSceneStruct.h"

int createInstance(void**out)
{
	if (out) {
		TextInSceneStruct*st = new TextInSceneStruct(nullptr);
		*out = dynamic_cast<AstStruct*>(st);
	}
	return 0;
}
void freeInstance(void*_)
{
	if (_) {
		TextInSceneStruct*st = (TextInSceneStruct*)_;
		delete st;
	}
}