﻿#pragma once

#include <QObject>
#include "GlobalDefine.h"

class TextInSceneReadLineFunction : public QObject, public AstFunction
{
	Q_OBJECT
private:
	class TextInSceneReadLineFunctionPrivate;
	TextInSceneReadLineFunctionPrivate*private_ptr;

public:
	TextInSceneReadLineFunction(QObject *parent);
	~TextInSceneReadLineFunction();

public:
	AstFunction* operator()();
	// AstFunction
public:
	virtual QString name() override;
	virtual int* outputTypes() override;
	virtual int outputCount() override;
	virtual int* inputTypes() override;
	virtual int inputCount() override;
	virtual void* function() override;

};
