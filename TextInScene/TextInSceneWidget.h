﻿#pragma once

#include <QWidget>
namespace Ui { class TextInSceneWidget; };

class QTextDocument;
class AstScene;
class TextInSceneWidget : public QWidget
{
	Q_OBJECT
private:
	class TextInSceneWidgetPrivate;
	TextInSceneWidgetPrivate*private_ptr;

public:
	TextInSceneWidget(QWidget *parent = Q_NULLPTR);
	~TextInSceneWidget();

protected slots:
	void on_btn_select_clicked();
	void on_btn_load_clicked();
public:
	AstScene* operator()();
	QTextDocument* textDocument();
private:
	Ui::TextInSceneWidget *ui;
};
