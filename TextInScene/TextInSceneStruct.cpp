﻿#include "TextInSceneStruct.h"

#include "TextInSceneWidget.h"

#include "TextInSceneReadLineFunction.h"
#include "TextInSceneRowCountFunction.h"

class TextInSceneStruct::TextInSceneStructPrivate :public QObject
{
public:
	TextInSceneReadLineFunction * readline_function;
	TextInSceneRowCountFunction * rowcount_function;
public:
	TextInSceneStructPrivate(QObject*parent):QObject(parent)
	{
		readline_function = new TextInSceneReadLineFunction(this);
		rowcount_function = new TextInSceneRowCountFunction(this);
	}
};

TextInSceneStruct::TextInSceneStruct(QObject *parent)
	: QObject(parent),private_ptr(new TextInSceneStructPrivate(this))
{
}

TextInSceneStruct::~TextInSceneStruct()
{
}

//////////////////////////////////////////////////////
//
QString TextInSceneStruct::sceneNames(int scene_type)
{
	if (AstScene::SCENE_IN == scene_type)
		return "TextInScene";

	return "";
}
bool TextInSceneStruct::hasScene(const QString&name, int scene_type)
{
	switch (scene_type) {
	case AstScene::SCENE_IN:
		if (0 == name.compare("TextInScene", Qt::CaseInsensitive))
			return true;
		break;
	}

	return false;
}
AstScene* TextInSceneStruct::createScene(const QString&name, int scene_type, AstStructContext*ctx)
{
	switch (scene_type) {
	case AstScene::SCENE_IN:
		if (0 == name.compare("TextInScene", Qt::CaseInsensitive)) {
			TextInSceneWidget*wgt = new TextInSceneWidget(nullptr);
			return wgt ? wgt->operator()() : nullptr;
		}
		break;
	}
	return nullptr;
}
void TextInSceneStruct::freeScene(AstScene*_)
{
	delete _->widget();
}
QString TextInSceneStruct::functionNames()
{
	return "TextInSceneRowCount,TextInSceneReadLine";
}
AstFunction* TextInSceneStruct::getFunction(const QString&name)
{
	if (0 == name.compare("TextInSceneRowCount", Qt::CaseSensitivity::CaseInsensitive)) {
		return private_ptr->rowcount_function->operator()();
	}
	if (0 == name.compare("TextInSceneReadLine", Qt::CaseSensitivity::CaseInsensitive)) {
		return private_ptr->readline_function->operator()();
	}
	return nullptr;
}
