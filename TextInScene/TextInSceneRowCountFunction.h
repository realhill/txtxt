﻿#pragma once

#include <QObject>
#include "GlobalDefine.h"

class TextInSceneRowCountFunction : public QObject, public AstFunction
{
	Q_OBJECT
private:
	class TextInSceneRowCountFunctionPrivate;
	TextInSceneRowCountFunctionPrivate*private_ptr;
public:
	TextInSceneRowCountFunction(QObject *parent);
	~TextInSceneRowCountFunction();

public:
	AstFunction* operator()();
	// AstFunction
public:
	virtual QString name() override;
	virtual int* outputTypes() override;
	virtual int outputCount() override;
	virtual int* inputTypes() override;
	virtual int inputCount() override;
	virtual void* function() override;

};
