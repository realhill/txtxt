﻿#pragma once
#include <QObject>

#include "AstObject.h"

class AstFunction : public AstObject
{
public:
	virtual char* objectID() override { return "AstFunction"; }
	//	virtual ~AstFunction(){}
	virtual QString name() = 0;
	virtual int* outputTypes() = 0;
	virtual int outputCount() = 0;
	virtual int* inputTypes() = 0;
	virtual int inputCount() = 0;
	virtual void* function() = 0;
};
