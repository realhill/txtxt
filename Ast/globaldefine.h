﻿#pragma once

#include "../Ast/AstInterface.h"
#include "../Ast/Ast.h"
//#include "../Ast/AstIn.h"
//#include "../Ast/AstOut.h"
//#include "../Ast/AstWorker.h"
//#include "../Ast/TableInScene.h"
//#include "../Ast/TextInScene.h"
#include "../lib/ast_error.h"
#include "../lib/data_struct.h"
#include "../lib/variant.h"
#include "../lib/function.h"
#include "../lib/astring.h"
