﻿#pragma once

#include <QObject>
#include "KTemplateT.h"

class AstWorker;
class AstWorkerImpl : public QObject
{
	Q_OBJECT
private:
	class AstWorkerImplPrivate;
	AstWorkerImplPrivate*private_ptr;

public:
	AstWorkerImpl(QObject *parent);
	~AstWorkerImpl();

	AstWorker* operator()();

	void setFunctions(void*funcs);

};

//////////////////////////////////////////////////////
//
class AstWorkerImplGroup : public KGroupVectorPtrT<AstWorkerImpl>
{
private:
	class AstWorkerImplGroupPrivate;
	AstWorkerImplGroupPrivate*private_ptr;

public:
	AstWorkerImplGroup(QObject*parent);

	AstWorkerImpl* createAstWorker();
	AstWorkerImpl* queryAstWorker(AstWorker*);
	void closeAstWorker(AstWorker*);
	void closeAstWorker(AstWorkerImpl*);
};
