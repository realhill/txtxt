﻿#include "LessFunction.h"

class LessFunction::LessFunctionPrivate :QObject
{
public:
	int in_params[16];
	int out_params[1];
	int in_count;
	int out_count;
public:
	LessFunctionPrivate(QObject*parent) :QObject(parent)
	{
		in_count = 2;
		out_count = 1;

		in_params[0] = FPT_NUMBER;
		in_params[1] = FPT_NUMBER;
		out_params[0] = FPT_NUMBER;
	}

};

static int Less_FUNCTION(struct variant_s *in, int in_count, struct variant_s *out, void*tag)
{
	if (in_count != 2) {
		return ERR_PARAM_NOT_MATCH;
	}

	if (!(VARIANTOBJ_TYPE(in[0]) & VALUETYPE_NUMBER)) {
		return ERR_DATATYPE_NOT_MATCH;
	}
	if (!(VARIANTOBJ_TYPE(in[1]) & VALUETYPE_NUMBER)) {
		return ERR_DATATYPE_NOT_MATCH;
	}

	double v0 = 0;
	double v1 = 0;

	v0 = (VARIANTOBJ_TYPE(in[0]) == VALUETYPE_DOUBLE ? in[0].u.d : in[0].u.i);
	v1 = (VARIANTOBJ_TYPE(in[1]) == VALUETYPE_DOUBLE ? in[1].u.d : in[1].u.i);

	out->type = VALUETYPE_INTEGER;
	out->u.i = (v0 < v1 ? 1 : 0);

	return ERR_OK;
}


LessFunction::LessFunction(QObject *parent)
	: QObject(parent),private_ptr(new LessFunctionPrivate(this))
{
}

LessFunction::~LessFunction()
{
}

QString LessFunction::name()
{
	return "<";
}
int* LessFunction::outputTypes()
{
	return private_ptr->out_params;
}
int LessFunction::outputCount()
{
	return private_ptr->out_count;
}
int* LessFunction::inputTypes()
{
	return private_ptr->in_params;
}
int LessFunction::inputCount()
{
	return private_ptr->in_count;
}
void* LessFunction::function()
{
	return Less_FUNCTION;
}
