﻿#pragma once

#include "AstScene.h"

class  AstWorker : public AstScene
{
public:
	virtual char*objectID() override { return "AstWorker"; }
	virtual int type() override { return AstScene::SCENE_WORKER; }
	virtual int parseString(const QString&) = 0;
	virtual int run() = 0;
};
