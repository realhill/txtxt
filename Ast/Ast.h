﻿#pragma once

#include <QObject>

# if defined(AST_LIB)
#  define AST_EXPORT Q_DECL_EXPORT
# else
#  define AST_EXPORT Q_DECL_IMPORT
# endif

class AstScene;
class AstWorker;
class AST_EXPORT Ast : QObject
{
	Q_OBJECT
private:
	class AstPrivate;
	AstPrivate*private_ptr;
public:
    Ast(QObject*parent = nullptr);
	int init(const QString&path);
public:

	QString sceneNames(int scene_type);
	AstScene* queryScene(int scene_type, const QString&label);
	AstScene* createScene(const QString&scene_name, int scene_type, const QString&label);
	void closeScene(AstScene*);
	void closeScene(int scene_type, const QString&label);

	//AstWorker* createWorker(const QString&name);
	//void closeWorker(AstWorker*);
};

