﻿#include "GlobalData.h"

#include "../lib/env.h"
#include "../lib/function.h"
#include "../lib/function_group.h"

static GlobalData global_data;

GlobalData::GlobalData(QObject *parent)
	: QObject(parent)
{
}

GlobalData::~GlobalData()
{
}

GlobalData* GlobalData::instance()
{
	return &global_data;
}

void GlobalData::init()
{
	// 
}