﻿#pragma once

#include <QObject>
#include "GlobalDefine.h"

class PlusFunction : public QObject, public AstFunction
{
	Q_OBJECT
private:
	class PlusFunctionPrivate;
	PlusFunctionPrivate*private_ptr;

public:
	PlusFunction(QObject *parent);
	~PlusFunction();

	// AstFunction
public:
	virtual QString name() override;
	virtual int* outputTypes() override;
	virtual int outputCount() override;
	virtual int* inputTypes() override;
	virtual int inputCount() override;
	virtual void* function() override;

};
