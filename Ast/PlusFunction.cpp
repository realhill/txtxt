﻿#include "PlusFunction.h"

class PlusFunction::PlusFunctionPrivate :QObject
{
public:
	int in_params[16];
	int out_params[1];
	int in_count;
	int out_count;
public:
	PlusFunctionPrivate(QObject*parent) :QObject(parent)
	{
		in_count = 2;
		out_count = 1;

		in_params[0] = FPT_NUMBER;
		in_params[1] = FPT_NUMBER;
		out_params[0] = FPT_NUMBER;
	}

};

static int Plus_FUNCTION(struct variant_s *in, int in_count, struct variant_s *out, void*tag)
{
	if (in_count != 2) {
		return ERR_PARAM_NOT_MATCH;
	}

	if (!(VARIANTOBJ_TYPE(in[0]) & VALUETYPE_NUMBER)) {
		return ERR_DATATYPE_NOT_MATCH;
	}
	if (!(VARIANTOBJ_TYPE(in[1]) & VALUETYPE_NUMBER)) {
		return ERR_DATATYPE_NOT_MATCH;
	}

	double v0 = 0;
	double v1 = 0;

	v0 = (VARIANTOBJ_TYPE(in[0]) == VALUETYPE_DOUBLE ? in[0].u.d : in[0].u.i);
	v1 = (VARIANTOBJ_TYPE(in[1]) == VALUETYPE_DOUBLE ? in[1].u.d : in[1].u.i);

	out->type = VALUETYPE_DOUBLE;
	out->u.d = v0 + v1;

	return ERR_OK;
}

PlusFunction::PlusFunction(QObject *parent)
	: QObject(parent),private_ptr(new PlusFunctionPrivate(this))
{
}

PlusFunction::~PlusFunction()
{
}

QString PlusFunction::name()
{
	return "+";
}
int* PlusFunction::outputTypes()
{
	return private_ptr->out_params;
}
int PlusFunction::outputCount()
{
	return private_ptr->out_count;
}
int* PlusFunction::inputTypes()
{
	return private_ptr->in_params;
}
int PlusFunction::inputCount()
{
	return private_ptr->in_count;
}
void* PlusFunction::function()
{
	return Plus_FUNCTION;
}
