﻿#pragma once

#include <QObject>

#include "../lib/env.h"
#include "../lib/function.h"
#include "../lib/function_group.h"

class GlobalData : public QObject
{
	Q_OBJECT
public:

public:
	GlobalData(QObject *parent=nullptr);
	~GlobalData();

	static GlobalData* instance();

public:
	void init();
};
