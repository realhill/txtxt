﻿#include "OpenSceneFunction.h"

class OpenSceneFunction::OpenSceneFunctionPrivate :QObject
{
public:
	int in_params[16];
	int out_params[1];
	int in_count;
	int out_count;
public:
	OpenSceneFunctionPrivate(QObject*parent) :QObject(parent)
	{
		in_count = 2;
		out_count = 1;

		in_params[0] = FPT_NUMBER;
		in_params[1] = FPT_STRING;
		out_params[0] = FPT_PTR;
	}

};

static int OpenScene_FUNCTION(struct variant_s *in, int in_count, struct variant_s *out, void*tag)
{
	char*name = nullptr;
	int type = 0;
	if (in[0].type & VALUETYPE_NUMBER) {
		type = (int)in[0].u.d;
	}
	if (in[1].type == VALUETYPE_STRING) {
		name = ((struct astring_s*)in[1].u.ptr)->value;
	}

	Ast*a = (Ast*)tag;
	AstScene*s = a->queryScene(type, name);

	out->type = VALUETYPE_VOID_PTR;
	out->u.ptr = (void*)s;

	return 0;
}

OpenSceneFunction::OpenSceneFunction(QObject *parent)
	: QObject(parent),private_ptr(new OpenSceneFunctionPrivate(this))
{
}

OpenSceneFunction::~OpenSceneFunction()
{
}

QString OpenSceneFunction::name()
{
	return "OpenScene";
}
int* OpenSceneFunction::outputTypes()
{
	return private_ptr->out_params;
}
int OpenSceneFunction::outputCount()
{
	return private_ptr->out_count;
}
int* OpenSceneFunction::inputTypes()
{
	return private_ptr->in_params;
}
int OpenSceneFunction::inputCount()
{
	return private_ptr->in_count;
}
void* OpenSceneFunction::function()
{
	return OpenScene_FUNCTION;
}
