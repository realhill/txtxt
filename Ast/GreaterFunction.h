﻿#pragma once

#include <QObject>
#include "GlobalDefine.h"

class GreaterFunction : public QObject, public AstFunction
{
	Q_OBJECT
private:
	class GreaterFunctionPrivate;
	GreaterFunctionPrivate*private_ptr;
public:
	GreaterFunction(QObject *parent);
	~GreaterFunction();

	// AstFunction
public:
	virtual QString name() override;
	virtual int* outputTypes() override;
	virtual int outputCount() override;
	virtual int* inputTypes() override;
	virtual int inputCount() override;
	virtual void* function() override;

};
