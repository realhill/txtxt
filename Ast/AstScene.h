﻿#pragma once
#include <QObject>

#include "AstObject.h"

class QWidget;
class AstScene : public AstObject
{
public:
	enum {
		SCENE_NONE = 0,
		SCENE_IN,
		SCENE_OUT,
		SCENE_WORKER,
		SCENE_OTHER
	};
public:
	virtual char*objectID() override { return "AstScene"; }
	virtual int type() = 0;
	virtual QString typeName() = 0;
	virtual QString label() = 0;
	virtual void setLabel(const QString&) = 0;
	virtual QWidget* widget() = 0;
	virtual void onOpening() = 0;
	virtual void onClosing() = 0;
};
