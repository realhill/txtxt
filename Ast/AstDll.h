﻿#pragma once

#include <QObject>
#include "KTemplateT.h"

class AstStruct;
class AstDll : public QObject
{
	Q_OBJECT
private:
	class AstDllPrivate;
	AstDllPrivate*private_ptr;
public:
	AstDll(QObject *parent);
	~AstDll();

	bool load(const QString&filename);

	AstStruct* astStruct();
};

class AstDllGroup : public KGroupPtrT<QString, AstDll>
{
private:
	class AstDllGroupPrivate;
	AstDllGroupPrivate*private_ptr;

public:
	AstDllGroup(QObject*parent = nullptr);

public:
	AstDll* load(const QString&filename);

	QString sceneNames(int scene_type);
	AstStruct* astStruct(const QString&name, int scene_type);
};
