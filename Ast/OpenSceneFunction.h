﻿#pragma once

#include <QObject>

#include "GlobalDefine.h"

class OpenSceneFunction : public QObject, public AstFunction
{
	Q_OBJECT
private:
	class OpenSceneFunctionPrivate;
	OpenSceneFunctionPrivate*private_ptr;

public:
	OpenSceneFunction(QObject *parent);
	~OpenSceneFunction();

	// AstFunction
public:
	virtual QString name() override;
	virtual int* outputTypes() override;
	virtual int outputCount() override;
	virtual int* inputTypes() override;
	virtual int inputCount() override;
	virtual void* function() override;
};
