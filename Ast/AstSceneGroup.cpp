﻿#include "AstSceneGroup.h"
#include "AstScene.h"

AstSceneGroup::AstSceneGroup(QObject *parent)
	: KGroupVectorPtrT<AstScene>(parent)
{
}

AstSceneGroup::~AstSceneGroup()
{
}

AstScene* AstSceneGroup::queryScene(int scene_type, const QString&label)
{
	for each(auto a in *items()) {
		if (a->type() == scene_type && !label.compare(a->label(), Qt::CaseInsensitive))
			return a;
	}
	return nullptr;
}

int AstSceneGroup::addScene(AstScene*_)
{
	addItem(_);
	return 0;
}
int AstSceneGroup::removeScene(AstScene*_)
{
	delItem(_, false);
	return 0;
}
