﻿#pragma once
#include <QObject>
#include <QVariant>
#include <QVector>

#include "AstObject.h"

class AstConstant
{
public:
	QString name;
	QVariant value;
};

class AstStructContext
{
public:
	void* function_group;
	void* constant_group;
};

class AstFunction;
class AstScene;
class AstStruct : public AstObject
{
public:
	virtual ~AstStruct() {}

	virtual char* objectID() override { return "AstStruct"; }
public:
	virtual QString sceneNames(int scene_type) = 0;
	virtual bool hasScene(const QString&name, int scene_type) = 0;
	virtual AstScene* createScene(const QString&name, int scene_type, AstStructContext*ctx) = 0;
	virtual void freeScene(AstScene*) = 0;
	virtual QString functionNames() = 0;
	virtual AstFunction* getFunction(const QString&name) = 0;
	virtual QVector<AstConstant> * constants() = 0;
};
