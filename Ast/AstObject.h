﻿#pragma once

class AstObject
{
public:
	virtual char* objectID() = 0;
};