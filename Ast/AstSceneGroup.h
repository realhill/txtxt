﻿#pragma once

#include <QObject>
#include "KTemplateT.h"

class AstScene;
class AstSceneGroup : public KGroupVectorPtrT<AstScene>
{
	Q_OBJECT

public:
	AstSceneGroup(QObject *parent = nullptr);
	~AstSceneGroup();

	AstScene* queryScene(int scene_type, const QString&label);
	int addScene(AstScene*);
	int removeScene(AstScene*);
};
