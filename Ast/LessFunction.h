﻿#pragma once

#include <QObject>
#include "GlobalDefine.h"

class LessFunction : public QObject, public AstFunction
{
	Q_OBJECT
private:
	class LessFunctionPrivate;
	LessFunctionPrivate*private_ptr;

public:
	LessFunction(QObject *parent);
	~LessFunction();

	// AstFunction
public:
	virtual QString name() override;
	virtual int* outputTypes() override;
	virtual int outputCount() override;
	virtual int* inputTypes() override;
	virtual int inputCount() override;
	virtual void* function() override;

};
