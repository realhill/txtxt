#pragma once

#include <QDialog>
namespace Ui { class KImportDialog; };

class KImportDialog : public QDialog
{
	Q_OBJECT

public:
	KImportDialog(QWidget *parent = Q_NULLPTR);
	~KImportDialog();

private:
	Ui::KImportDialog *ui;
};
