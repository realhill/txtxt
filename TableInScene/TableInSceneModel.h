﻿#pragma once

#include <QObject>
#include <QStandardItemModel>
#include <QStringList>
#include <QVector>

class TableInSceneModel : public QStandardItemModel
{
	Q_OBJECT
private:
	class TableInSceneModelPrivate;
	TableInSceneModelPrivate*private_ptr;
public:
	TableInSceneModel(QObject *parent);
	~TableInSceneModel();

public:
	void appendModelData(QVector< QVector<QVariant>> *, bool recalc);
	void appendModelData(QVector<QVariant>*, bool recalc);
	QVector< QVector<QVariant>> * modelData();
	QVector<QVariant>* rowDataOfIdx(int idx);
	int readColData(int idx, QVector<QVariant>*);
	int colIndexByName(const QString&);
	void clearModelData();

	void setHeaderTitle(const QStringList&);
	void syncRows();
	void refresh(int row/* -1 */);
public:
	Qt::ItemFlags flags(const QModelIndex &index) const override;
	// 头数据
	QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;

	// 逻辑数据
	QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
	bool setData(const QModelIndex &index, const QVariant &value, int role = Qt::EditRole) override;

};
