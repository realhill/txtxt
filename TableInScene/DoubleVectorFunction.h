﻿#pragma once

#include <QObject>
#include "GlobalDefine.h"

class DoubleVectorFunction : public QObject, public AstFunction
{
	Q_OBJECT
private:
	class DoubleVectorFunctionPrivate;
	DoubleVectorFunctionPrivate*private_ptr;
public: 
	DoubleVectorFunction(QObject *parent);
	~DoubleVectorFunction();

public:
	AstFunction* operator()();
	// AstFunction
public:
	virtual QString name() override;
	virtual int* outputTypes() override;
	virtual int outputCount() override;
	virtual int* inputTypes() override;
	virtual int inputCount() override;
	virtual void* function() override;
};
