#pragma once

#include <QtCore/qglobal.h>

#ifndef BUILD_STATIC
# if defined(TABLEINSCENE_LIB)
#  define TABLEINSCENE_EXPORT Q_DECL_EXPORT
# else
#  define TABLEINSCENE_EXPORT Q_DECL_IMPORT
# endif
#else
# define TABLEINSCENE_EXPORT
#endif
