﻿#include "DoubleVectorFunction.h"

#include <QDebug>

#include "DoubleVector.h"

class DoubleVectorFunction::DoubleVectorFunctionPrivate :QObject
{
public:
	int in_params[16];
	int out_params[1];
	int in_count;
	int out_count;
public:
	DoubleVectorFunctionPrivate(QObject*parent) :QObject(parent)
	{
		in_count = 0;
		out_count = 1;

		in_params[0] = FPT_PTR;
		in_params[1] = FPT_NUMBER;
		out_params[0] = FPT_VECTOR;
	}

};

DoubleVectorFunction::DoubleVectorFunction(QObject *parent)
	: QObject(parent),private_ptr(new DoubleVectorFunctionPrivate(this))
{
}

DoubleVectorFunction::~DoubleVectorFunction()
{
}

static int DoubleVectorFunction_FUNCTION(struct variant_s *in, int in_count, struct variant_s *out, void*tag)
{
	if (0 != in_count)
		return ERR_PARAM_NOT_MATCH;

	DoubleVector*pv = new DoubleVector(nullptr);
	out->type = VALUETYPE_DOUBLE_VECTOR;
	out->u.ptr = (IVector*)pv;
	out->fn_addref = BaseVector::addref_fn; //addref_doublevector;
	out->fn_release = BaseVector::release_fn;//release_doublevector;

	qDebug() << "double vector:" << out->u.ptr << ", fn:" << BaseVector::addref_fn << "," << BaseVector::release_fn;

	return 0;
}

AstFunction* DoubleVectorFunction::operator()()
{
	return dynamic_cast<AstFunction*>(this);
}

QString DoubleVectorFunction::name()
{
	return "DoubleVector";
}
int* DoubleVectorFunction::outputTypes()
{
	return private_ptr->out_params;
}
int DoubleVectorFunction::outputCount()
{
	return private_ptr->out_count;
}
int* DoubleVectorFunction::inputTypes()
{
	return private_ptr->in_params;
}
int DoubleVectorFunction::inputCount()
{
	return private_ptr->in_count;
}
void* DoubleVectorFunction::function()
{
	return DoubleVectorFunction_FUNCTION;
}
