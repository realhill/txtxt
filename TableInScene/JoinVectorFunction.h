﻿#pragma once

#include <QObject>
#include "GlobalDefine.h"

class JoinVectorFunction : public QObject, public AstFunction
{
	Q_OBJECT
private:
	class JoinVectorFunctionPrivate;
	JoinVectorFunctionPrivate*private_ptr;

public:
	JoinVectorFunction(QObject *parent);
	~JoinVectorFunction();

public:
	AstFunction* operator()();
	// AstFunction
public:
	virtual QString name() override;
	virtual int* outputTypes() override;
	virtual int outputCount() override;
	virtual int* inputTypes() override;
	virtual int inputCount() override;
	virtual void* function() override;

};
