﻿#pragma once

#include <QObject>
#include <QVector>
#include <QVariant>

class ImportSetting
{
public:
	QChar field_sep;		// 字段分隔符
	int row_sep_mode;		// 记录分隔符，0=CRLF, 1=CR, 2=LF
	QString codec_name;		// 编码名称
	QChar txt_sep;			// 文本识别符
public:
	ImportSetting()
	{
		row_sep_mode = 0;
		txt_sep = '\"';
		field_sep = ',';
		codec_name = "utf-8";
	}
};

class ImportUtils
{
public:
	static void importRow(const QString&, QVector<QVariant>*, ImportSetting*);
	static void importRow(const QString&, QVector<QVariant>*, QChar sep);
	static void importRowToDouble(const QString&, QVector<QVariant>*, QChar sep);
};
