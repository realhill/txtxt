﻿#include "TableInScene.h"

#include "TableInSceneStruct.h"

int createInstance(void**out)
{
	if (out) {
		TableInSceneStruct*st = new TableInSceneStruct(nullptr);
		*out = dynamic_cast<AstStruct*>(st);
	}
	return 0;
}
void freeInstance(void*_)
{
	if (_) {
		TableInSceneStruct*st = (TableInSceneStruct*)_;
		delete st;
	}
}
