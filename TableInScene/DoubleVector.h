﻿#pragma once

#include <QObject>
#include <QVector>
#include <QVariant>

#include "BaseVector.h"

class DoubleVector : public BaseVector
{
	Q_OBJECT

public:
	DoubleVector(QObject *parent);
	~DoubleVector();
private:
	virtual int type() override;
};
