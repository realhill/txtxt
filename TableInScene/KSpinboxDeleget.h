﻿#pragma once

#include <QStyledItemDelegate>
#include <QItemDelegate>
#include <QStyleOption>
#include <QPainter>

class KSpinboxDeleget : public QItemDelegate
{
	Q_OBJECT
public:
	int doublespin_decimals;
public:
	KSpinboxDeleget(QObject *parent);
	~KSpinboxDeleget();

	QWidget* createEditor(QWidget*parent, const QStyleOptionViewItem&option, const QModelIndex&index) const Q_DECL_OVERRIDE;
	//void setEditorData(QWidget*editor, const QModelIndex&index) const Q_DECL_OVERRIDE;
	void setModelData(QWidget*editor, QAbstractItemModel*model, const QModelIndex&index) const Q_DECL_OVERRIDE;
	//void updateEditorGeometry(QWidget*editor, const QStyleOptionViewItem&option, const QModelIndex&index) const Q_DECL_OVERRIDE;
	void paint(QPainter*painter, const QStyleOptionViewItem&option, const QModelIndex&index) const Q_DECL_OVERRIDE;
private slots:
	void commitAndCloseEditor();

};
