﻿#pragma once

#include <QObject>
#include "GlobalDefine.h"

class TableInSceneStruct : public QObject, public AstStruct
{
	Q_OBJECT
private:
	class TableInSceneStructPrivate;
	TableInSceneStructPrivate*private_ptr;
public:
	TableInSceneStruct(QObject *parent);
	~TableInSceneStruct();

	virtual QString sceneNames(int scene_type) override;
	virtual bool hasScene(const QString&name, int scene_type) override;
	virtual AstScene* createScene(const QString&name, int scene_type, AstStructContext*ctx) override;
	virtual void freeScene(AstScene*) override;

	virtual QString functionNames() override;
	virtual AstFunction* getFunction(const QString&name) override;
	virtual QVector<AstConstant>* constants() override { return nullptr; }
};
