﻿#include "ImportUtils.h"

void ImportUtils::importRow(const QString&, QVector<QVariant>*, ImportSetting*)
{

}

void ImportUtils::importRow(const QString&_, QVector<QVariant>*out, QChar sep)
{
	QStringList sl = _.split(sep);

	int size = out->size();
	out->resize(size + sl.count());

	for (int i = 0; i < sl.count(); i++) {
		out->operator[](size + i) = sl[i];
	}
}

void ImportUtils::importRowToDouble(const QString&_, QVector<QVariant>*out, QChar sep)
{
	QStringList sl = _.split(sep);

	int size = out->size();
	out->resize(size + sl.count());

	for (int i = 0; i < sl.count(); i++) {
		out->operator[](size + i).setValue(sl[i].toDouble());
	}
}
