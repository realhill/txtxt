﻿#include "TableInSceneModel.h"


class TableInSceneModel::TableInSceneModelPrivate :public QObject
{
public:
	QStringList headers;
	QVector<QVector<QVariant>> datas;
public:
	TableInSceneModelPrivate(QObject*parent):QObject(parent)
	{
		datas.append(QVector<QVariant>(5, 23.8));
		datas.append(QVector<QVariant>(5, 23.8));
		datas.append(QVector<QVariant>(5, 23.8));
		datas.append(QVector<QVariant>(5, 23.8));
		datas.append(QVector<QVariant>(6, "abcd"));
		datas.append(QVector<QVariant>(6, QStringLiteral("可以")));
	}
	void setCellData(const QModelIndex &index, const QVariant&value)
	{
		if (index.row() >= datas.count())
			return;

		QVector<QVariant>*row = &datas[index.row()];

		if (index.column() >= row->count()) {
			row->resize(index.column() + 1);
		}

		row->operator[](index.column()) = value;
	}
	int columnIdxByName()
	{
		return 0;
	}
	int colCountByData()
	{
		int temp, r = headers.count();
		int n = datas.count();
		for (int i = 0; i < n; i++) {
			temp = datas[i].count();
			if (r < temp)
				r = temp;
		}
		return r;
	}
	void recalc()
	{
		int r = colCountByData();
		((TableInSceneModel*)parent())->setColumnCount(r);
	}

};

TableInSceneModel::TableInSceneModel(QObject *parent)
	: QStandardItemModel(parent),private_ptr(new TableInSceneModelPrivate(this))
{
	private_ptr->recalc();
}

TableInSceneModel::~TableInSceneModel()
{
}

void TableInSceneModel::appendModelData(QVector< QVector<QVariant>> *_, bool recalc)
{
	private_ptr->datas.append(*_);
	if (recalc)
		private_ptr->recalc();
}
void TableInSceneModel::appendModelData(QVector<QVariant>*_, bool recalc)
{
	private_ptr->datas.append(*_);
	if (recalc)
		private_ptr->recalc();
}
QVector< QVector<QVariant>> * TableInSceneModel::modelData()
{
	return &private_ptr->datas;
}
QVector<QVariant>* TableInSceneModel::rowDataOfIdx(int idx)
{
	if (idx >= private_ptr->datas.count())
		return nullptr;
	return &private_ptr->datas[idx];
}
int TableInSceneModel::readColData(int idx, QVector<QVariant>*out)
{
	if (idx < 0 || idx >= columnCount())
		return -1;
		

	int count = private_ptr->datas.count();
	out->resize(count);
	for (int i = 0; i < count; i++) {
		if (idx < private_ptr->datas[i].count()) {
			out->operator[](i) = private_ptr->datas[i][idx];
		}
	}
	return 0;
}
int TableInSceneModel::colIndexByName(const QString&name)
{
	int n = columnCount();
	for (int i = 0; i < n; i++) {
		QString s = headerData(n, Qt::Orientation::Horizontal).toString();
		if (0 == s.compare(name, Qt::CaseSensitivity::CaseInsensitive))
			return i;
	}
	return -1;
}
void TableInSceneModel::clearModelData()
{
	private_ptr->datas.clear();
	private_ptr->recalc();
}

void TableInSceneModel::setHeaderTitle(const QStringList&sl)
{
	private_ptr->headers = sl;
	private_ptr->recalc();
	//setColumnCount(sl.count());
}
/////////////////////////
void TableInSceneModel::syncRows()
{
	int rows = rowCount();
	int count = private_ptr->datas.count();

	if (rows == count)
		return;

	if (rows < count) {
		for (int i = rows; i < count; i++) {
			appendRow(new QStandardItem);
		}
	}
	else {
		removeRows(0, rows - count);
	}
}
void TableInSceneModel::refresh(int row /* =-1 */)
{
	int count = columnCount();

	if (row >= 0) {
		emit dataChanged(index(row, 0), index(row, count - 1), QVector<int>(1, Qt::DisplayRole));
	}
	else {
		emit dataChanged(index(0, 0), index(rowCount() - 1, count - 1), QVector<int>(1, Qt::DisplayRole));
	}
}

Qt::ItemFlags TableInSceneModel::flags(const QModelIndex &index) const
{
	if (!index.isValid())
		return Qt::ItemIsEnabled | Qt::ItemIsSelectable;

	//if (COLUMN_POLICYNAME == index.column())
	//	return Qt::ItemIsEnabled | Qt::ItemIsSelectable | Qt::ItemIsEditable;
	//if (COLUMN_NAME == index.column() || COLUMN_CONTRACTINFO == index.column())
	//	return Qt::ItemIsEnabled | Qt::ItemIsSelectable;
	return Qt::ItemIsEnabled | Qt::ItemIsSelectable | Qt::ItemIsEditable;
}

QVariant TableInSceneModel::headerData(int section, Qt::Orientation orientation, int role/* = Qt::DisplayRole*/) const
{
	if (role == Qt::TextAlignmentRole) {
		return (int)(Qt::AlignCenter);
	}

	if (role != Qt::DisplayRole)
		return QVariant();

	if (orientation == Qt::Orientation::Horizontal) {

		if (section < private_ptr->headers.count()) {
			return private_ptr->headers[section];
		}
		return QStringLiteral("%1").arg(section+1);
	}

	return QVariant(section + 1);
}


QVariant TableInSceneModel::data(const QModelIndex &index, int role) const
{
	if (!index.isValid())
		return QVariant();

	switch (role) {
	case Qt::TextAlignmentRole:
		if (index.column() >= 0)
			return int(Qt::AlignCenter);
		return QStandardItemModel::data(index, role);

		//return int(Qt::AlignLeft | Qt::AlignVCenter);
	case Qt::DisplayRole:
		do {
			if (index.row() >= private_ptr->datas.count())
				return "";
			if (index.column() >= private_ptr->datas[index.row()].count())
				return "";

			return private_ptr->datas[index.row()][index.column()];
		} while (0);
	case Qt::BackgroundRole:
		if (0 == (index.row() % 2))
			return QColor(250, 250, 250);//QColor(Qt::lightGray);

		return QStandardItemModel::data(index, role);// QColor(Qt::green);
	case Qt::ForegroundRole:
		do {
			//KCombinationOrder*comb_order = nullptr;
			//switch (index.column()) {
			//case COLUMN_ENABLE:
			//	//comb_order = KGlobalData::orderGroup()->item(index.row());// v.toString());
			//	//if (comb && comb->enable()) {
			//	//	return QColor(Qt::red);
			//	//}
			//	break;
			//case COLUMN_POLICYNAME:
			//	//do {
			//	//	comb_order = KGlobalData::orderGroup()->item(index.row());// v.toString());
			//	//	if (comb_order) {
			//	//		KPolicy*policy = KGlobalData::policyGroup()->item(comb->policyName());
			//	//		if (!policy || !policy->enable())
			//	//			return QColor(Qt::red);
			//	//	}
			//	//	//
			//	//} while (0);
			//	break;
			//}
			return QStandardItemModel::data(index, role);
		} while (0);
		//else if (role == Qt::SizeHintRole)
		//{
		//	KCombination*comb = KGlobalData::combinationGroup()->item(index.row());
		//	int count = comb->contractCount();

		//	int n = ((QTableView*)parent())->verticalHeader()->defaultSectionSize();
		//	return QSize(((QTableView*)parent())->visualRect(index).width(), (count > 1 ? count : 1) * n);
		//}
	case Qt::EditRole:
		do {
			do {
				if (index.row() >= private_ptr->datas.count())
					return QVariant();
				if (index.column() >= private_ptr->datas[index.row()].count())
					return QVariant();

				return private_ptr->datas[index.row()][index.column()];
			} while (0);
		} while (0);
		break;
	}

	return QStandardItemModel::data(index, role);
}

bool TableInSceneModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
	//KCombinationOrder*comb_order = KGlobalData::orderGroup()->item(index.row());
	//if (!comb_order)
	//	return false;

	//if (index.column() == COLUMN_POLICYNAME) {
	//	comb->setPolicyName(value.toString());
	//	emit dataChanged(index, index);
	//	return true;
	//}
	private_ptr->setCellData(index, value);
	return true;
	//emit dataChanged(index, index);
	//return QStandardItemModel::setData(index, value, role);
}
