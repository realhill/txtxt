﻿#include "DoubleVector.h"

#include <QDebug>


DoubleVector::DoubleVector(QObject *parent)
	: BaseVector(parent)
{
}

DoubleVector::~DoubleVector()
{
	qDebug() << "DoubleVector release:" << (void*)this;
}

int DoubleVector::type()
{
	return VECTORTYPE_DOUBLE;
}
