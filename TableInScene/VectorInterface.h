﻿#pragma once

#include <QObject>
#include <QVector>
#include <QVariant>

class IVector:public QObject
{
public:
	IVector(QObject*parent) :QObject(parent) {}
	virtual ~IVector() {}

	virtual QVector<QVariant>*data() = 0;
	virtual int type() = 0;
	// 返回添加有效项计数
	virtual int append(QVector<QVariant>*) = 0;
	// 返回添加有效项计数
	virtual int append(const QVariant&v) = 0;
	virtual int count() = 0;
	virtual void remove(int idx) = 0;
	virtual QVariant get(int idx) = 0;
	// 返回设置有效项计数
	virtual int set(int idx, const QVariant&) = 0;
	virtual int addRef() = 0;
	virtual int release() = 0;
};
