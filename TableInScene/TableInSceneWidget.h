﻿#pragma once

#include <QWidget>
namespace Ui { class TableInSceneWidget; };

class TableInSceneModel;
class AstScene;
class QTableView;
class TableInSceneWidget : public QWidget
{
	Q_OBJECT
private:
	class TableInSceneWidgetPrivate;
	TableInSceneWidgetPrivate*private_ptr;
public:
	TableInSceneWidget(QWidget *parent = Q_NULLPTR);
	~TableInSceneWidget();

	void initTable();

private:
public slots:
	void onMenuSetCellToDouble();
	void onMenuSetCellToText();
	void onMenuSetCellToEmpty();
	void onMenuCopyCells();
	void onMenuPasteCells();
	void onTableContextMenu(const QPoint&pt);
	void on_btn_import_clicked();
public:
	AstScene* operator()();
	TableInSceneModel*model();
	QTableView* tableView();
private:
	Ui::TableInSceneWidget *ui;
};
