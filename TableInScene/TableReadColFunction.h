﻿#pragma once

#include <QObject>
#include "GlobalDefine.h"

class TableReadColFunction : public QObject, public AstFunction
{
	Q_OBJECT

private:
	class TableReadColFunctionPrivate;
	TableReadColFunctionPrivate*private_ptr;
public:
	TableReadColFunction(QObject *parent);
	~TableReadColFunction();

public:
	AstFunction* operator()();
	// AstFunction
public:
	virtual QString name() override;
	virtual int* outputTypes() override;
	virtual int outputCount() override;
	virtual int* inputTypes() override;
	virtual int inputCount() override;
	virtual void* function() override;

};
