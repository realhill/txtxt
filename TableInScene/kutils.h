﻿#ifndef KUTILS_H
#define KUTILS_H

#include <QObject>
#include <QLine>
#include <QColor>
#include <QVector>
#include <QVariant>
#include <QDatetime>



class KUtils
{
public:
    // 四舍五入，保留dot_num位小数
    static float round(float v, int dot_num);
	// 四舍五入，保留dot_num位小数
	static double round(double v, int dot_num);

    /**
    1. 数字比万小的，原样输出
    2. 数字介于万和亿之间的，以万为单位处理，不包括单位万但包括小数点，只能保留4位。小数点后位数多的话，按四舍五入。注意条件：包括小数点前面的、小数点、小数点后面的，只能4位
    如： 123500  结果应该是12.4万
    3.数字到达亿的，以亿为单位处理，规则同2
     * @brief human
     * @param v
     * @return
     */
    static QString human(float v, int dot_num);
	static QString human(double v, int dot_num);
	// 点到直线的距离的平方
	static qreal distance2(const QPointF& pt, const QPointF& p1,const QPointF&p2);
	// 点到直线的距离的平方
	static qreal distance2(const QPointF& pt, const QLineF& line);
	// 点到直线的距离
	static qreal distance(const QPointF& pt, const QLineF& line);
	// 点到点的距离的平方
	static qreal distance2(const QPointF&pt, const QPointF&p1);
	// 点到点的距离
	static qreal distance(const QPointF&pt, const QPointF&p1);
	// 直线上取一点
	static void getLinePoint(const QPointF&p1, const QPointF&p2, qreal _x, QPointF&out);
	// 计算垂足
	static void calcFootPoint(const QPointF&pt1, const QPointF&pt2, const QPointF&point, QPointF&out);
	/**
	 * 判断垂点在线段上的位置， pt1------------------pt2
	 * 返回值：0 = 在线段上， - 1 = 在pt2~pt1延长线上，1 = 在pt1~pt2延长线上
	 */
	static int testFootPoint(const QPointF&pt1, const QPointF&pt2, const QPointF&foot);
	static float gradient(const QPointF&p0, const QPointF&p1);

	// 点到线段的距离（点积）
	static qreal pt2LineSegmentDistance(const QPointF&p, const QPointF& a, const QPointF&b);
	// 点到直线距离（叉积）
	static qreal pt2LineDistance(const QPointF&p, const QPointF& a, const QPointF&b);
	//
	static qreal pt2LineDistance(const QPointF&p, const QPointF& a, const QPointF&b, bool is_segment);
	static bool testIntersectPos(QLineF A, QLineF B, QPointF*out);//返回AB与CD交点，无交点返回（0,0）
	static bool testIntersectPos(QPointF posA, QPointF posB, QPointF posC, QPointF posD, QPointF*out);//返回AB与CD交点，无交点返回（0,0）

	// 最小价位转最小精度
	static double minPrecision(int dot_digits);
	static double base(double v);
	static double floor(double v, double significance);
	static double ceil(double v, double significance);

	static QColor invertColor(const QColor &crColor);
	static QColor changeXORColor(QColor crColor, QColor crBkColor);

	static QDate kdateOf(const QString&d);
	static QTime ktimeOf(const QString&t);
	static QDateTime kdatetimeOf(const QString&d, const QString&t);
	static int   kdayOf(const QString&d1, const QString&d2);


};

#endif // KUNTILS_H
