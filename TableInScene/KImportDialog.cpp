#include "KImportDialog.h"
#include "ui_KImportDialog.h"

KImportDialog::KImportDialog(QWidget *parent)
	: QDialog(parent)
{
	ui = new Ui::KImportDialog();
	ui->setupUi(this);
}

KImportDialog::~KImportDialog()
{
	delete ui;
}
