﻿#pragma once

#include <QObject>

#include "VectorInterface.h"

class BaseVector : public IVector
{
	Q_OBJECT
private:
	class BaseVectorPrivate;
	BaseVectorPrivate*private_ptr;
public:
	enum {
		VECTORTYPE_NONE = 0,
		VECTORTYPE_DOUBLE,
	};
public:
	static int addref_fn(void*_);
	static int release_fn(void*_);
public:
	BaseVector(QObject*parent);
	virtual ~BaseVector();

	virtual QVector<QVariant>*data() override;
	virtual int type() override;
	// 返回添加有效项计数
	virtual int append(QVector<QVariant>*) override;
	// 返回添加有效项计数
	virtual int append(const QVariant&v) override;
	virtual int count() override;
	virtual void remove(int idx) override;
	virtual QVariant get(int idx) override;
	// 返回设置有效项计数
	virtual int set(int idx, const QVariant&) override;
	virtual int addRef() override;
	virtual int release() override;
};