﻿#include "TableInSceneStruct.h"

#include "TableInSceneWidget.h"
#include "DoubleVectorFunction.h"
#include "TableReadColFunction.h"
#include "JoinVectorFunction.h"

#define SCENE_NAME "TableInScene"

class TableInSceneStruct::TableInSceneStructPrivate :public QObject
{
public:
	DoubleVectorFunction * doublevector_function;
	TableReadColFunction * tablereadcol_function;
	JoinVectorFunction* joinvector_function;
public:
	TableInSceneStructPrivate(QObject*parent) :QObject(parent)
	{
		doublevector_function = new DoubleVectorFunction(this);
		tablereadcol_function = new TableReadColFunction(this);
		joinvector_function = new JoinVectorFunction(this);
	}
};

TableInSceneStruct::TableInSceneStruct(QObject *parent)
	: QObject(parent), private_ptr(new TableInSceneStructPrivate(this))
{
}

TableInSceneStruct::~TableInSceneStruct()
{
}

//////////////////////////////////////////////////////
//
QString TableInSceneStruct::sceneNames(int scene_type)
{
	if (AstScene::SCENE_IN == scene_type)
		return SCENE_NAME;

	return "";
}
bool TableInSceneStruct::hasScene(const QString&name, int scene_type)
{
	switch (scene_type) {
	case AstScene::SCENE_IN:
		if (0 == name.compare(SCENE_NAME, Qt::CaseInsensitive))
			return true;
		break;
	}

	return false;
}
AstScene* TableInSceneStruct::createScene(const QString&name, int scene_type, AstStructContext*ctx)
{
	switch (scene_type) {
	case AstScene::SCENE_IN:
		if (0 == name.compare(SCENE_NAME, Qt::CaseInsensitive)) {
			TableInSceneWidget*wgt = new TableInSceneWidget(nullptr);
			return wgt ? wgt->operator()() : nullptr;
		}
		break;
	}
	return nullptr;
}
void TableInSceneStruct::freeScene(AstScene*_)
{
	delete _->widget();
}
QString TableInSceneStruct::functionNames()
{
	QStringList sl;
	sl.append(private_ptr->doublevector_function->name());
	sl.append(private_ptr->tablereadcol_function->name());
	sl.append(private_ptr->joinvector_function->name());

	return sl.join(",");
}
AstFunction* TableInSceneStruct::getFunction(const QString&name)
{
	if (0 == name.compare(private_ptr->doublevector_function->name(), Qt::CaseSensitivity::CaseInsensitive)) {
		return private_ptr->doublevector_function->operator()();
	}
	if (0 == name.compare(private_ptr->tablereadcol_function->name(), Qt::CaseSensitivity::CaseInsensitive)) {
		return private_ptr->tablereadcol_function->operator()();
	}
	if (0 == name.compare(private_ptr->joinvector_function->name(), Qt::CaseSensitivity::CaseInsensitive)) {
		return private_ptr->joinvector_function->operator()();
	}
	return nullptr;
}
