﻿#pragma once

#include <QWidget>
namespace Ui { class WorkerWidget; };

class AstWorker;
class WorkerWidget : public QWidget
{
	Q_OBJECT
private:
	class WorkerWidgetPrivate;
	WorkerWidgetPrivate*private_ptr;
public:
	WorkerWidget(QWidget *parent = Q_NULLPTR);
	~WorkerWidget();

	AstWorker* operator()();

	void setEnvFunctions(void*funcs);
	void setEnvConstants(void*cons);

private slots:
	void on_btn_compile_clicked();
	void on_btn_exec_clicked();
	void on_edt_script_modificationChanged(bool);
	void on_edt_script_textChanged();

private:
	Ui::WorkerWidget *ui;
};
