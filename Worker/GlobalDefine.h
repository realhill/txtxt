﻿#pragma once

#include "../Ast/AstInterface.h"

#include "../lib/ast_error.h"
#include "../lib/data_struct.h"
#include "../lib/variant.h"
#include "../lib/function.h"
