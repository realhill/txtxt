﻿#include "WorkerStruct.h"

#include "WorkerWidget.h"

class WorkerStruct::WorkerStructPrivate :public QObject
{
public:

public:
	WorkerStructPrivate(QObject*parent) :QObject(parent)
	{
	}
};

WorkerStruct::WorkerStruct(QObject *parent)
	: QObject(parent)
{
}

WorkerStruct::~WorkerStruct()
{
}

//////////////////////////////////////////////////////
//
QString WorkerStruct::sceneNames(int scene_type)
{
	if (AstScene::SCENE_WORKER == scene_type)
		return "WorkerScene";

	return "";
}
bool WorkerStruct::hasScene(const QString&name, int scene_type)
{
	switch (scene_type) {
	case AstScene::SCENE_WORKER:
		if (0 == name.compare("WorkerScene", Qt::CaseInsensitive))
			return true;
		break;
	}

	return false;
}
AstScene* WorkerStruct::createScene(const QString&name, int scene_type, AstStructContext*ctx)
{
	switch (scene_type) {
	case AstScene::SCENE_WORKER:
		if (0 == name.compare("WorkerScene", Qt::CaseInsensitive)) {
			WorkerWidget*wgt = new WorkerWidget(nullptr);
			wgt->setEnvFunctions(ctx->function_group);
			wgt->setEnvConstants(ctx->constant_group);
			return wgt ? wgt->operator()() : nullptr;
		}
		break;
	}
	return nullptr;
}
void WorkerStruct::freeScene(AstScene*_)
{
	delete _->widget();
}
QString WorkerStruct::functionNames()
{
	return "";// "TextInSceneRowCount,TextInSceneReadLine";
}
AstFunction* WorkerStruct::getFunction(const QString&name)
{
	//if (0 == name.compare("TextInSceneRowCount", Qt::CaseSensitivity::CaseInsensitive)) {
	//	return private_ptr->rowcount_function->operator()();
	//}
	//if (0 == name.compare("TextInSceneReadLine", Qt::CaseSensitivity::CaseInsensitive)) {
	//	return private_ptr->readline_function->operator()();
	//}
	return nullptr;
}
