﻿#include "Worker.h"

#include "WorkerStruct.h"

int createInstance(void**out)
{
	if (out) {
		WorkerStruct*st = new WorkerStruct(nullptr);
		*out = dynamic_cast<AstStruct*>(st);
	}
	return 0;
}
void freeInstance(void*_)
{
	if (_) {
		WorkerStruct*st = (WorkerStruct*)_;
		delete st;
	}
}
