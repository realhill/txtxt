﻿#pragma once

#include <QObject>
#include "GlobalDefine.h"

class WorkerStruct : public QObject, public AstStruct
{
	Q_OBJECT
private:
	class WorkerStructPrivate;
	WorkerStructPrivate*private_ptr;
public:
	WorkerStruct(QObject *parent);
	~WorkerStruct();

	virtual QString sceneNames(int scene_type) override;
	virtual bool hasScene(const QString&name, int scene_type) override;
	virtual AstScene* createScene(const QString&name, int scene_type, AstStructContext*ctx) override;
	virtual void freeScene(AstScene*) override;
	virtual QString functionNames() override;
	virtual AstFunction* getFunction(const QString&name) override;
	virtual QVector<AstConstant> * constants() override { return nullptr; }
};
